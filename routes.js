let users = require('./controller/employee');
let auth = require('./controller/auth');
let employee = require('./controller/employee');
let notification = require('./controller/notification');
let profile = require('./controller/profile');
let reminder = require('./controller/reminder');
let promotion = require('./controller/promotion');
let retire = require('./controller/retire');
let calendar = require('./controller/calendar');
let dashboard = require('./controller/dashbaord');
let board = require('./controller/information_board');
let documents = require('./controller/documents');
let report = require('./controller/report');
let middleware = require('./middlewares/middleware-token');
let gcsMiddlewares = require('./middlewares/google-cloud-storage');

var MailConfig = require('./config/email');
var hbs = require('nodemailer-express-handlebars');
var gmailTransport = MailConfig.GmailTransport;


async function routes(fastify, options) {

    fastify.get('/email', function (request, reply) {
        MailConfig.ViewOption(gmailTransport, hbs);
        let HelperOptions = {
            from: '"Kemnaker TU" <kemnaker.tu@gmail.com>',
            to: 'juveticsatu@gmail.com',
            subject: 'TEDASDASD!',
            template: 'email',
            context: {
                name: "DOLPO ANGSA",
                email: "kemnaker.tu@gmail.com",
                address: "ASDASDASDASDASD"
            }
        };
        gmailTransport.sendMail(HelperOptions, (error, info) => {
            if (error) {
                console.log(error);
                reply.send({ message: error, code: 400 });
            }
            console.log("email is send");
            console.log(info);
            reply.send({ code: 200, message: 'Email sent!', values: info });
        });
    });

    //Route Ujicoba
    fastify.get('/', function (request, reply) {
        reply.send({ message: 'ASUP OJIG', code: 200 });
    });

    /**
     * Authentication
     */
    fastify.put('/api/auth/activation', auth.activateAccount);
    fastify.put('/api/auth/create_password', auth.createPassword);
    fastify.post('/api/auth/login', auth.login);
    fastify.route({
        method: 'PUT',
        url: '/api/auth/logout',
        preHandler: async function (request, reply, done) {
            await middleware.check(request, reply);
            done()
        },
        handler: auth.logout
    });
    fastify.route({
        method: 'GET',
        url: '/api/auth/admin/data',
        preHandler: async function (request, reply, done) {
            await middleware.check(request, reply);
            done()
        },
        handler: auth.getAuthAdmin
    });
    fastify.route({
        method: 'GET',
        url: '/api/auth/employee/data',
        preHandler: async function (request, reply, done) {
            await middleware.check(request, reply);
            done()
        },
        handler: auth.getAuthEmployee
    });

    /**
     * Notification
     */
    fastify.route({
        method: 'GET',
        url: '/api/notif/admin',
        preHandler: async function (request, reply, done) {
            await middleware.check(request, reply);
            done()
        },
        handler: notification.showNotifAdmin
    });
    fastify.route({
        method: 'GET',
        url: '/api/notif/employee',
        preHandler: async function (request, reply, done) {
            await middleware.check(request, reply);
            done()
        },
        handler: notification.showNotifPegawai
    });
    fastify.route({
        method: 'PUT',
        url: '/api/notif/employee/read',
        preHandler: async function (request, reply, done) {
            await middleware.check(request, reply);
            done()
        },
        handler: notification.readNotifPegawai
    });
    fastify.route({
        method: 'POST',
        url: '/api/notif/admin/read',
        preHandler: async function (request, reply, done) {
            await middleware.check(request, reply);
            done()
        },
        handler: notification.readNotifAdmin
    });
    fastify.route({
        method: 'POST',
        url: '/api/notif/admin/valid',
        preHandler: async function (request, reply, done) {
            await middleware.check(request, reply);
            done()
        },
        handler: notification.validNotifAdmin
    });
    fastify.route({
        method: 'POST',
        url: '/api/notif/admin/reminder',
        preHandler: async function (request, reply, done) {
            await middleware.check(request, reply);
            done()
        },
        handler: notification.sendNotifReminder
    });

    /**
     * Profile
     */
    fastify.route({
        method: 'GET',
        url: '/api/profile/get',
        preHandler: async function (request, reply, done) {
            await middleware.check(request, reply);
            done()
        },
        handler: profile.getMyProfile
    });
    fastify.route({
        method: 'GET',
        url: '/api/profile/get/nip',
        preHandler: async function (request, reply, done) {
            await middleware.check(request, reply);
            done()
        },
        handler: profile.getProfileByNip
    });
    fastify.route({
        method: 'PUT',
        url: '/api/profile/update',
        preHandler: async function (request, reply, done) {
            await middleware.check(request, reply);
            done()
        },
        handler: profile.editProfile
    });

    fastify.route({
        method: 'PUT',
        url: '/api/profile/files',
        preHandler: async function (request, reply, done) {
            await middleware.check(request, reply);
            done()
        },
        handler: profile.updateProfileFiles
    });

    /**
     * Employee
     */
    fastify.route({
        method: 'GET',
        url: '/api/employees',
        preHandler: async function (request, reply, done) {
            await middleware.check(request, reply);
            done()
        },
        handler: employee.getAllEmployee
    });
    fastify.route({
        method: 'GET',
        url: '/api/employee/nip',
        preHandler: async function (request, reply, done) {
            await middleware.check(request, reply);
            done()
        },
        handler: employee.getEmployeeByNip
    });
    fastify.route({
        method: 'POST',
        url: '/api/employee/add',
        preHandler: async function (request, reply, done) {
            await middleware.check(request, reply);
            done()
        },
        handler: employee.addPegawai
    });
    fastify.route({
        method: 'POST',
        url: '/api/employee/update',
        preHandler: async function (request, reply, done) {
            await middleware.check(request, reply);
            done()
        },
        handler: employee.updatePegawai
    });
    fastify.route({
        method: 'POST',
        url: '/api/employee/delete',
        preHandler: async function (request, reply, done) {
            await middleware.check(request, reply);
            done()
        },
        handler: employee.deletePegawai
    });
    fastify.route({
        method: 'GET',
        url: '/api/role/get',
        preHandler: async function (request, reply, done) {
            await middleware.check(request, reply);
            done()
        },
        handler: employee.getRole
    });
    fastify.route({
        method: 'GET',
        url: '/api/unit/get',
        preHandler: async function (request, reply, done) {
            await middleware.check(request, reply);
            done()
        },
        handler: employee.getUnit
    });
    fastify.route({
        method: 'GET',
        url: '/api/golongan/get',
        preHandler: async function (request, reply, done) {
            await middleware.check(request, reply);
            done()
        },
        handler: employee.getGrade
    });
    fastify.route({
        method: 'POST',
        url: '/api/unit/get',
        preHandler: async function (request, reply, done) {
            await middleware.check(request, reply);
            done()
        },
        handler: employee.addUnit
    });
    fastify.route({
        method: 'PUT',
        url: '/api/employee/position_type/update',
        preHandler: async function (request, reply, done) {
            await middleware.check(request, reply);
            done()
        },
        handler: employee.updatePositionTypeByNip
    });

    /**
     * Reminder
     */
    fastify.route({
        method: 'POST',
        url: '/api/reminder/send',
        preHandler: async function (request, reply, done) {
            await middleware.check(request, reply);
            done()
        },
        handler: reminder.sendReminder
    });
    fastify.route({
        method: 'POST',
        url: '/api/reminder/valid',
        preHandler: async function (request, reply, done) {
            await middleware.check(request, reply);
            done()
        },
        handler: reminder.sendValidNotif
    });

    /**
     * Promotion
     */
    fastify.route({
        method: 'GET',
        url: '/api/promotion/get',
        preHandler: async function (request, reply, done) {
            await middleware.check(request, reply);
            done()
        },
        handler: promotion.getUpcomingPromotion
    });

    /**
     * Retire
     */
    fastify.route({
        method: 'GET',
        url: '/api/retire/get',
        preHandler: async function (request, reply, done) {
            await middleware.check(request, reply);
            done()
        },
        handler: retire.getUpcomingRetire
    });

    /**
     * Dashboard
     */
    fastify.route({
        method: 'GET',
        url: '/api/dashboard/birthday/get',
        preHandler: async function (request, reply, done) {
            await middleware.check(request, reply);
            done()
        },
        handler: dashboard.getBirthdayThisMonth
    });
    fastify.route({
        method: 'GET',
        url: '/api/dashboard/promotion/get',
        preHandler: async function (request, reply, done) {
            await middleware.check(request, reply);
            done()
        },
        handler: dashboard.getPromotionThisMonth
    });
    fastify.route({
        method: 'GET',
        url: '/api/dashboard/retire/get',
        preHandler: async function (request, reply, done) {
            await middleware.check(request, reply);
            done()
        },
        handler: dashboard.getRetireThisMonth
    });

    /**
     * Google Calendar
     */
    fastify.get('/api/calendar/get', calendar.getCalendarEvents);

    /**
     * Information Board
     */
    fastify.get('/api/board/running_text/get', board.getRunningText);
    fastify.post('/api/board/running_text/add', board.addRunningText);
    fastify.put('/api/board/running_text/update', board.updateRunningText);
    fastify.post('/api/board/running_text/delete', board.deleteRunningText);
    fastify.get('/api/board/news/get', board.getFlashNews);
    fastify.post('/api/board/news/add', board.addFlashNews);
    fastify.put('/api/board/news/update', board.updateFlashNews);
    fastify.post('/api/board/news/delete', board.deleteFlashNews);
    fastify.put('/api/board/youtube/update', board.setYoutubeUrl);
    fastify.get('/api/board/youtube/get', board.getYoutubeUrl);

    /**
     * Documents
     */
    fastify.route({
        method: 'POST',
        url: '/api/documents_promotion/upload',
        preHandler: async function (request, reply, done) {
            await middleware.check(request, reply);
            done()
        },
        handler: documents.updateDocuments
    });

    fastify.route({
        method: 'GET',
        url: '/api/documents_promotion/get',
        preHandler: async function (request, reply, done) {
            await middleware.check(request, reply);
            done()
        },
        handler: documents.getDocumentsByNip
    });

    fastify.post('/testupload', function (req, reply) {
        console.log('DIR DIR DIR DIR ------ ' + __dirname);
        const files = req.raw.files.modol
        console.log(files)

        files.mv(__dirname+ '/storage/' + files.name, function(err) {
        if (err)
            reply.send({ message: err, code: 500 });
        console.log('Uploaded! ' + files.name);
        });
        
        reply.send(files)
    });

    /**
     * Report Employee
     */
    fastify.route({
        method: 'GET',
        url: '/api/report',
        preHandler: async function (request, reply, done) {
            await middleware.check(request, reply);
            done()
        },
        handler: report.getEmployeeReport
    });
}

module.exports = routes;