let response = require('../response');
let connection = require('../connection');
let header = require('../helpers/token');
let moment = require('moment');
let crypto = require('crypto');
let sha1 = require('sha1');

var MailConfig = require('../config/email');
var hbs = require('nodemailer-express-handlebars');
var gmailTransport = MailConfig.GmailTransport;

const key = crypto.randomBytes(32);
const iv = crypto.randomBytes(16);

function encrypt(text) {
    var cipher = crypto.createCipher('aes-256-cbc', 'd6F3Efeq')
    var crypted = cipher.update(text, 'utf8', 'hex')
    crypted += cipher.final('hex');
    return crypted;
}

function decrypt(text) {
    var decipher = crypto.createDecipher('aes-256-cbc', 'd6F3Efeq')
    var dec = decipher.update(text, 'hex', 'utf8')
    dec += decipher.final('utf8');
    return dec;
}

async function login(request, reply) {

    let now = moment().format('YYYY-MM-DD HH:mm:ss').toString();

    let pg_nip = request.body.nip;
    let pg_password = request.body.password;
    console.log(`YAH CIK: ${request.body}`);
    console.log(`YAH CIK: ${pg_nip} ${pg_password}`);
    let sql_user = `SELECT * FROM t_pegawai WHERE pg_nip = ?`;

    let pg_id = -1;
    let secret_id = await getSecret();

    let data_user = await new Promise((resolve) =>
        connection.query(sql_user, [pg_nip], function (error, rows) {
            if (error) {
                console.log(error);
                return response.badRequest('asup query', `${error}`, reply)
            }

            console.log(`USER BEUNANG.... ${rows.length} ${JSON.stringify(rows)}`);

            if (rows.length > 0) {
                // let verify = sha1(pg_password) === rows[0].pg_password;
                let verify = pg_password === rows[0].pg_password;
                pg_id = rows[0].pg_id;
                let user_data = {
                    name: rows[0].pg_full_name,
                    first_name: rows[0].pg_first_name, 
                    last_name: rows[0].pg_last_name,
                    picture: rows[0].pg_profile_pic,
                    nip: rows[0].pg_nip,
                    email: rows[0].pg_email,
                    token: rows[0].pg_token,
                    role: rows[0].rl_id,
                    gender: rows[0].pg_gender
                };

                return verify ? resolve(user_data) : resolve(false);
            }
            else {
                return resolve(false);
            }
        })
    );

    if (!data_user) {
        console.log("NIP/Password Salah!")
        return response.badRequest('aa', 'NIP atau password yang anda masukkan salah!', reply)
    }

    //Menciptakan random string sebanyak kurang lebih 20 karakter
    let id = crypto.randomBytes(25).toString('hex');

    let created_at = now;
    let updated_at = now;
    //Membuat tanggal hari ini + 7 hari kedepan untuk masa aktif token.
    let expires_at = moment().add(7, 'days').format('YYYY-MM-DD HH:mm:ss').toString();

    let sql_auth = `INSERT INTO t_authentication (id, pg_id, secret_id, expires_at, created_at, updated_at) 
                VALUES(?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE expires_at = ?, updated_at = ?`;
    let data_auth = await new Promise((resolve) =>
        connection.query(sql_auth,
            [id, pg_id, secret_id, expires_at, created_at, updated_at, expires_at, updated_at], function (error, rows) {
                console.log(`INSERT NEW TOKEN = ${JSON.stringify(rows)}`);

                if (error) {
                    console.log(`FAIL INSERT ${error}`);
                    return response.badRequest('asup insert', `${error}`, reply)
                }

                let array = {
                    name: data_user.name,
                    first_name: data_user.first_name, 
                    last_name: data_user.last_name,
                    picture: data_user.picture,
                    nip: data_user.nip,
                    email: data_user.email,
                    role_id: data_user.role,
                    gender: data_user.gender,
                    token: id,
                    expires_at: expires_at
                };

                return resolve(array);
            })
    );

    console.log(`OJIG SAVE AUTH`);

    console.log(`SENDING EMAIL ACTIVATION`);

    return response.ok(data_auth, `Berhasil login!`, reply);
}

async function activateAccount(request, reply) {
    let now = moment().format('YYYY-MM-DD HH:mm:ss').toString();
    let status = "Aktivasi";

    let sql = "";
    let body = "";

    if (request.body.nip == null || request.body.nip == "") {
        sql = `UPDATE t_pegawai set pg_status_account = ?, updated_at = ? WHERE pg_email = ?`;
        body = request.body.email;
    } else {
        sql = `UPDATE t_pegawai set pg_status_account = ?, updated_at = ? WHERE pg_nip = ?`;
        body = request.body.nip;
    }

    let data = await new Promise((resolve) =>
        connection.query(sql,
            [status, now, body], function (error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                let token_data = {};
                if (request.body.nip == null || request.body.nip == "") {
                    token_data = {
                        activation_token: encrypt(request.body.email)
                    }
                } else {
                    token_data = {
                        activation_token: encrypt(request.body.nip)
                    }
                }
                
                console.log("AFFECTED " + rows.affectedRows);

                return rows.affectedRows > 0 ? resolve(token_data) : resolve({});
            })
    );

    var msg = "";
    if (data.activation_token == null || data.activation_token == "") {
        msg = "NIP or Email not found!";
    } else {
        msg = "Activation request success!";
        MailConfig.ViewOption(gmailTransport,hbs);
        let HelperOptions = {
            from: '"Kemnaker KKHI Admin" <kemnaker.tu@gmail.com>',
            to: request.body.email,
            subject: "Aktivasi Akun Kemnaker KKHI",
            template: 'email',
            context: {
                title: "Aktivasi Akun Kemnaker TU",
                name: request.body.nip,
                token: data.activation_token,
                email: request.body.email,
            }
        };
        gmailTransport.sendMail(HelperOptions, (error,info) => {
            if(error) {
            console.log(error);
            reply.send({ message: error, code: 400 });
            }
            console.log("email is send");
            console.log(info);
            reply.send({code: 200, message: 'Email sent!', values: info});
        });
    }
    return response.ok(data, msg, reply);
}

async function createPassword(request, reply) {
    let body = decrypt(request.body.token);
    console.log("NIP OJIG " + body);
    let password = request.body.password;
    let status = "Aktif";

    let now = moment().format('YYYY-MM-DD HH:mm:ss').toString();

    let sql = "";
    if (body.includes("@")) {
        sql = `UPDATE t_pegawai set pg_password = ?, pg_status_account = ?, updated_at = ? WHERE pg_email = ?`;
    } else {
        sql = `UPDATE t_pegawai set pg_password = ?, pg_status_account = ?, updated_at = ? WHERE pg_nip = ?`;
    }

    let data = await new Promise((resolve) =>
        connection.query(sql,
            [password, status, now, body], function (error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                console.log("AFFECTED UPDATE PASSWORD " + rows.affectedRows);

                return rows.affectedRows > 0 ? resolve(true) : resolve(false);
            })
    );

    let msg = data ? "Password created!" : "Failed to create password!";
    return response.ok(data, msg, reply);
}

async function logout(request, reply) {

    let now = moment().format('YYYY-MM-DD HH:mm:ss').toString();
    let token = request.headers.authorization;
    let updated_at = now;
    let expires_at = now;

    let check = await header.check(token, reply);
    let sql = `UPDATE t_authentication set expires_at = ?, updated_at = ? WHERE pg_id = ?`;

    let data = await new Promise((resolve) =>
        connection.query(sql,
            [expires_at, updated_at, check.pg_id], function (error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                return rows.affectedRows > 0 ? resolve(true) : resolve(false);
            })
    );

    let msg = data ? "Berhasil logout!" : "Gagal logout!";
    return response.ok(data, msg, reply);
}

async function createToken(request, reply) {

    let now = moment().format('YYYY-MM-DD HH:mm:ss').toString();
    let secret = request.body.secret;
    let token = request.body.token;
    let created_at = now;
    let updated_at = now;

    //Mengambil id pengguna
    let pg_id = await getUserId(token);

    //Mengambil id secret key
    let secret_id = await getSecret(secret);

    //Kedua id harus ada di tiap table, kalau tidak ada salah satu maka harus dilemparkan message error.
    if (!secret_id || !user_id) {
        return response.badRequest('', 'Token atau Secret key kamu salah!', reply);
    }

    //Menciptakan random string sebanyak kurang lebih 20 karakter
    let id = crypto.randomBytes(25).toString('hex');

    //Membuat tanggal hari ini + 7 hari kedepan untuk masa aktif token.
    let expires_at = moment().add(7, 'days').format('YYYY-MM-DD HH:mm:ss').toString();

    let sql = `INSERT INTO t_authentication (id, pg_id, secret_id, expires_at, created_at, updated_at) 
                VALUES(?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE expires_at = ?, updated_at = ?`;
    let data = await new Promise((resolve) =>
        connection.query(sql,
            [id, pg_id, secret_id, expires_at, created_at, updated_at, expires_at, updated_at], function (error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                let array = {
                    token: id,
                    expires_at: expires_at
                };

                return resolve(array);
            })
    );

    return response.ok(data, `Berhasil membuat autentikasi!`, reply);
}

async function getUserId(token) {
    return new Promise((resolve) =>
        connection.query('SELECT pg_id FROM t_pegawai WHERE pg_token = ?', [token], function (error, rows) {
            if (error) {
                console.log(error);
            }

            return rows.length > 0 ? resolve(rows[0].id) : resolve(false);
        })
    );
}

async function getSecret() {
    return new Promise((resolve) =>
        connection.query('SELECT id FROM t_secret', function (error, rows) {
            if (error) {
                console.log(error);
            }

            return rows.length > 0 ? resolve(rows[0].id) : resolve(false);
        })
    );
}

async function checkToken(request, reply) {
    let token = request.body.token.toString();
    let user_token = request.body.user_token.toString();
    let now = moment().format('YYYY-MM-DD HH:mm:ss').toString();

    let sql = `SELECT t_authentication.*, t_pegawai.pg_token FROM t_authentication 
    INNER JOIN t_pegawai ON t_pegawai.pg_id = t_authentication.pg_id WHERE t_authentication.id = ? AND t_pegawai.pg_token = ?`;
    let data = await new Promise((resolve) =>
        connection.query(sql, [token, user_token], function (error, rows) {
            if (error) {
                console.log(error);
                return response.badRequest('', `${error}`, reply)
            }

            if (rows.length > 0) {
                return rows[0].pg_token === user_token ? resolve(rows[0].expires_at) : resolve(false);
            }
            else {
                return response.badRequest({}, "Token yang kamu masukkan salah!", reply);
            }

        })
    );

    let array = { expires_at: data };
    let message = moment(data).format('YYYY-MM-DD HH:mm:ss').toString() > now ?
        'Token ini milikmu dan masih aktif hingga saat ini!' : "Token kamu sudah tidak aktif!";

    return array ? response.ok(array, message, reply) : response.badRequest({}, message, reply);
}

async function getAuthAdmin(request, reply) {
    let token = request.headers.authorization;
    let check = await header.check(token, reply);

    let sql_count_admin = `SELECT count(na_id) as unread FROM t_notif_admin WHERE na_status_read = "0"`;

    let data_count_admin = await new Promise((resolve) =>
        connection.query(sql_count_admin,
            [],
            function(error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                return rows.length > 0 ? resolve(rows[0].unread) : resolve(0);
            })
    );

    let sql = `SELECT pg_nip, pg_full_name, pg_email, pg_first_name, pg_last_name, pg_profile_pic FROM t_pegawai WHERE pg_id = ?`;

    let data = await new Promise((resolve) =>
        connection.query(sql,
            [check.pg_id],
            function(error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }
                
                let user_data = {
                    nip: rows[0].pg_nip,
                    first_name: rows[0].pg_first_name, 
                    last_name: rows[0].pg_last_name,
                    picture: rows[0].pg_profile_pic,
                    name: rows[0].pg_full_name,
                    email: rows[0].pg_email,
                    notif_unread: data_count_admin
                }

                return rows.length > 0 ? resolve(user_data) : resolve({});
            })
    );

    return response.ok(data, '', reply);
}

async function getAuthEmployee(request, reply) {
    let token = request.headers.authorization;
    let check = await header.check(token, reply);

    let sql_count_emp = `SELECT count(np_id) as unread FROM t_notif_pegawai WHERE np_status_read = "0" AND pg_id = ?`;

    let data_count_emp = await new Promise((resolve) =>
        connection.query(sql_count_emp,
            [check.pg_id],
            function(error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }
    
                return rows.length > 0 ? resolve(rows[0].unread) : resolve(0);
            })
    );

    let sql = `SELECT pg_nip, pg_full_name, pg_email, pg_first_name, pg_last_name, pg_profile_pic FROM t_pegawai WHERE pg_id = ?`;

    let data = await new Promise((resolve) =>
        connection.query(sql,
            [check.pg_id],
            function(error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }
                
                let user_data = {
                    nip: rows[0].pg_nip,
                    first_name: rows[0].pg_first_name, 
                    last_name: rows[0].pg_last_name,
                    picture: rows[0].pg_profile_pic,
                    name: rows[0].pg_full_name,
                    email: rows[0].pg_email,
                    notif_unread: data_count_emp
                }

                return rows.length > 0 ? resolve(user_data) : resolve({});
            })
    );

    return response.ok(data, '', reply);
}

module.exports = {
    createToken, checkToken, login, logout, createPassword, activateAccount, getAuthAdmin, getAuthEmployee
};