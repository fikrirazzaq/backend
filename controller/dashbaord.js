let response = require('../response');
let connection = require('../connection');
let sha1 = require('sha1');
let moment = require('moment');
let crypto = require('crypto');
let header = require('../helpers/token');

async function getBirthdayThisMonth(request, reply) {
    let sql = `SELECT p.pg_nip, p.pg_full_name, p.pg_first_name, p.pg_last_name, p.pg_first_degree, p.pg_last_degree, 
    p.pg_profile_pic, p.pg_date_birthday, s.su_name, u.un_name,pa.pa_name, pa.pa_keputusan, pa.pa_date, gr.gr_name, gr.gr_group, 
    ga.ga_date
    FROM t_pegawai p
    JOIN t_sub_unit s USING (su_id)
    JOIN t_unit u USING (un_id)
    JOIN t_position_assignment pa USING (pg_id)
    JOIN t_grade_assignment ga USING (pg_id)
    JOIN t_grade gr USING (gr_id)
    WHERE MONTH(pg_date_birthday) = MONTH(NOW())`;

    let data = await new Promise((resolve) => connection.query(sql, [], function (error, rows) {
        if (error) {
            console.log(error);
            return response.badRequest('', `${error}`, reply)
        }

        if (rows.length > 0) {
            let array = [];
            rows.forEach(element => {
                array.push({
                    nip: element.pg_nip,
                    name: element.pg_full_name,
                    first_name: element.pg_first_name,
                    last_name: element.pg_last_name,
                    first_degree: element.pg_first_degree,
                    last_degree: element.pg_last_degree,
                    picture: element.pg_profile_pic,
                    birthday: element.pg_date_birthday,
                    unit: element.un_name,
                    sub_unit: element.su_name,
                    jabatan: {
                        nama: element.pa_name,
                        keputusan: element.pa_keputusan,
                        tanggal: element.pa_date
                    },
                    golongan: {
                        nama: element.gr_name,
                        ruang: element.gr_group,
                        tanggal: element.ga_date
                    },
                });
            });

            return resolve(array);
        }
        else {
            return resolve([]);
        }
    })
    );

    return response.ok(data, '', reply);
}

async function getPromotionThisMonth(request, reply) {
    let sql = `SELECT p.pg_nip, p.pg_full_name, p.pg_first_name, p.pg_last_name, p.pg_first_degree, p.pg_last_degree, 
    p.pg_profile_pic, p.pg_date_promotion_next, s.su_name, u.un_name,pa.pa_name, pa.pa_keputusan, pa.pa_date, gr.gr_name, gr.gr_group, 
    ga.ga_date
    FROM t_pegawai p
    JOIN t_sub_unit s USING (su_id)
    JOIN t_unit u USING (un_id)
    JOIN t_position_assignment pa USING (pg_id)
    JOIN t_grade_assignment ga USING (pg_id)
    JOIN t_grade gr USING (gr_id)
    WHERE MONTH(pg_date_promotion_next) = MONTH(NOW())`;

    let data = await new Promise((resolve) => connection.query(sql, [], function (error, rows) {
        if (error) {
            console.log(error);
            return response.badRequest('', `${error}`, reply)
        }

        if (rows.length > 0) {
            let array = [];
            rows.forEach(element => {
                array.push({
                    nip: element.pg_nip,
                    name: element.pg_full_name,
                    first_name: element.pg_first_name,
                    last_name: element.pg_last_name,
                    first_degree: element.pg_first_degree,
                    last_degree: element.pg_last_degree,
                    picture: element.pg_profile_pic,
                    promotion: element.pg_date_promotion_next,
                    unit: element.un_name,
                    sub_unit: element.su_name,
                    jabatan: {
                        nama: element.pa_name,
                        keputusan: element.pa_keputusan,
                        tanggal: element.pa_date
                    },
                    golongan: {
                        nama: element.gr_name,
                        ruang: element.gr_group,
                        tanggal: element.ga_date
                    },
                });
            });

            return resolve(array);
        }
        else {
            return resolve([]);
        }
    })
    );

    return response.ok(data, '', reply);
}

async function getRetireThisMonth(request, reply) {
    let sql = `SELECT p.pg_nip, p.pg_full_name, p.pg_first_name, p.pg_last_name, p.pg_first_degree, p.pg_last_degree, 
    p.pg_profile_pic, p.pg_date_retire, s.su_name, u.un_name, pa.pa_name, pa.pa_keputusan, pa.pa_date, gr.gr_name, gr.gr_group, 
    ga.ga_date
    FROM t_pegawai p
    JOIN t_sub_unit s USING (su_id)
    JOIN t_unit u USING (un_id)
    JOIN t_position_assignment pa USING (pg_id)
    JOIN t_grade_assignment ga USING (pg_id)
    JOIN t_grade gr USING (gr_id)
    WHERE MONTH(pg_date_retire) = MONTH(NOW())`;

    let data = await new Promise((resolve) => connection.query(sql, [], function (error, rows) {
        if (error) {
            console.log(error);
            return response.badRequest('', `${error}`, reply)
        }

        if (rows.length > 0) {
            let array = [];
            rows.forEach(element => {
                array.push({
                    nip: element.pg_nip,
                    name: element.pg_full_name,
                    first_name: element.pg_first_name,
                    last_name: element.pg_last_name,
                    first_degree: element.pg_first_degree,
                    last_degree: element.pg_last_degree,
                    picture: element.pg_profile_pic,
                    retire: element.pg_date_retire,
                    unit: element.un_name,
                    sub_unit: element.su_name,
                    jabatan: {
                        nama: element.pa_name,
                        keputusan: element.pa_keputusan,
                        tanggal: element.pa_date
                    },
                    golongan: {
                        nama: element.gr_name,
                        ruang: element.gr_group,
                        tanggal: element.ga_date
                    },
                });
            });

            return resolve(array);
        }
        else {
            return resolve([]);
        }
    })
    );

    return response.ok(data, '', reply);
}

module.exports = {
    getBirthdayThisMonth, getPromotionThisMonth, getRetireThisMonth
};