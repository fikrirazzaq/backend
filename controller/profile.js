let response = require('../response');
let connection = require('../connection');
let sha1 = require('sha1');
let moment = require('moment');
let crypto = require('crypto');
let header = require('../helpers/token');
let path = require('path');

async function getMyProfile(request, reply) {
    let token = request.headers.authorization;
    let check = await header.check(token, reply);
    let sql = `SELECT p.pg_position_type, s.su_name, u.un_name, p.pg_full_name, p.pg_address, p.pg_blood_type, p.pg_nik, p.pg_first_name, p.pg_last_name, p.pg_first_degree, p.pg_last_degree,
    p.pg_email, p.pg_nip, p.pg_birth_place, p.pg_date_birthday, pa.pa_name, pa.pa_keputusan, pa.pa_date, gr.gr_name, gr.gr_group, 
    ga.ga_date, p.pg_religion, p.pg_marital_status, p.pg_gender, p.pg_num_of_child, p.pg_date_promotion_next,
    p.pg_date_retire,
    p.pg_profile_pic, p.pg_paid_leave, p.pg_ktp, p.pg_kk, p.pg_npwp, p.pg_ijazah, p.pg_kartu_pegawai
    FROM t_pegawai p
    JOIN t_sub_unit s USING (su_id)
    JOIN t_unit u USING (un_id)
    JOIN t_position_assignment pa USING (pg_id)
    JOIN t_grade_assignment ga USING (pg_id)
    JOIN t_grade gr USING (gr_id)
    WHERE pg_id = ?`;

    console.log(check.pgId);
    let data = await new Promise((resolve) =>
        connection.query(sql,
            [check.pg_id], function (error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                let user_data = {
                    name: rows[0].pg_full_name,
                    profile_pic: rows[0].pg_profile_pic,
                    ktp: rows[0].pg_ktp,
                    kk: rows[0].pg_kk,
                    npwp: rows[0].pg_npwp,
                    ijazah: rows[0].pg_ijazah,
                    kartu_pegawai: rows[0].pg_kartu_pegawai,
                    cuti: rows[0].pg_paid_leave,
                    full_name: rows[0].pg_full_name,
                    first_name: rows[0].pg_first_name,
                    last_name: rows[0].pg_last_name,
                    first_degree: rows[0].pg_first_degree,
                    last_degree: rows[0].pg_last_degree,
                    blood_type: rows[0].pg_blood_type,
                    address: rows[0].pg_address,
                    nik: rows[0].pg_nik,
                    nip: rows[0].pg_nip,
                    email: rows[0].pg_email,
                    role: rows[0].rl_id,
                    unit: rows[0].un_name,
                    sub_unit: rows[0].su_name,
                    birth_place: rows[0].pg_birth_place,
                    birth_date: rows[0].pg_date_birthday,
                    gender: rows[0].pg_gender,
                    religion: rows[0].pg_religion,
                    marital_status: rows[0].pg_marital_status,
                    num_of_child: rows[0].pg_num_of_child,
                    jabatan_nama: rows[0].pa_name,
                    jabatan_keputusan: rows[0].pa_keputusan,
                    jabatan_tanggal: rows[0].pa_date,
                    golongan_nama: rows[0].gr_name,
                    golongan_ruang: rows[0].gr_group,
                    golongan_tanggal: rows[0].ga_date,
                    promotion_date: rows[0].pg_date_promotion_next,
                    retire_date: rows[0].pg_date_retire,
                    position_type: rows[0].pg_position_type
                };

                return rows.length > 0 ? resolve(user_data) : resolve({});
            })
    );

    let sql_education = `SELECT ed_degree as degree, ed_major as major, ed_institution as institution, ed_year as year FROM t_education 
        JOIN t_pegawai p USING (pg_id) WHERE pg_id = ?`;

    let data_education = await new Promise((resolve) =>
        connection.query(sql_education,
            [check.pg_id], function (error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                console.log("DATA PEGAWAI BY ID " + JSON.stringify(data) + "\n");
                console.log("DATA PEGAWAI BY ID " + data + "\n");
                console.log("DATA PEGAWAI BY ID " + data.jabatan_nama + "\n");

                return rows.length > 0 ? resolve(rows) : resolve({});
            })
    );

    let sql_diklat = `SELECT cf.cf_name as name, ct.ct_year as year FROM t_certification ct JOIN t_certificate cf USING (cf_id) WHERE pg_id = ?`;

    let diklat_data = await new Promise((resolve) =>
        connection.query(sql_diklat,
            [check.pg_id],
            function(error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                return rows.length > 0 ? resolve(rows) : resolve({});
            })
    );

    let sql_reminder_promotion = `SELECT rm_is_remindered, rm_status_reminder FROM t_reminder WHERE pg_id = ? AND rm_type = "1"`;

    let reminder_data_promotion = await new Promise((resolve) =>
        connection.query(sql_reminder_promotion,
            [check.pg_id],
            function(error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                return rows.length > 0 ? resolve(rows[0]) : resolve([]);
            })
    );

    let sql_reminder_retirement = `SELECT rm_is_remindered, rm_status_reminder FROM t_reminder WHERE pg_id = ? AND rm_type = "2"`;

    let reminder_data_retirement = await new Promise((resolve) =>
        connection.query(sql_reminder_retirement,
            [check.pg_id],
            function(error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                return rows.length > 0 ? resolve(rows[0]) : resolve([]);
            })
    );

    let user_data = {
        name: data.name,
        picture: data.profile_pic,
        ktp: data.ktp,
        kk: data.kk,
        npwp: data.npwp,
        kartu_pegawai: data.kartu_pegawai,
        ijazah: data.ijazah,
        cuti: data.cuti,
        full_name: data.full_name,
        first_name: data.first_name,
        last_name: data.last_name,
        first_degree: data.first_degree,
        last_degree: data.last_degree,
        blood_type: data.blood_type,
        address: data.address,
        nik: data.nik,
        nip: data.nip,
        email: data.email,
        role: data.role,
        unit: data.unit,
        sub_unit: data.sub_unit,
        birth_place: data.birth_place,
        birth_date: data.birth_date,
        gender: data.gender,
        religion: data.religion,
        marital_status: data.marital_status,
        num_of_child: data.num_of_child,
        jabatan: {
            nama: data.jabatan_nama,
            keputusan: data.jabatan_keputusan,
            tanggal: data.jabatan_tanggal
        },
        golongan: {
            nama: data.golongan_nama,
            ruang: data.golongan_ruang,
            tanggal: data.golongan_tanggal
        },
        education: data_education,
        diklat: diklat_data,
        date_promotion: data.promotion_date,
        date_retire: data.retire_date,
        reminder_promotion_sent: reminder_data_promotion.rm_is_remindered == 0 ? false : true,
        reminder_retirement_sent: reminder_data_retirement.rm_is_remindered == 0 ? false : true,
        position_type: data.position_type
    };

    return response.ok(user_data, '', reply);
}

async function getProfileByNip(request, reply) {
    let nip = request.query.nip;
    console.log("ASDJASLKDJLAKSJDLKASD " + nip);

    let sqlPgId = `SELECT pg_id FROM t_pegawai WHERE pg_nip = ?`;

    let dataPgId = await new Promise((resolve) =>
        connection.query(sqlPgId,
            [nip],
            function(error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                return rows.length > 0 ? resolve(rows[0].pg_id) : resolve('');
            })
    );

    console.log('PG ID - NIP ' + dataPgId + " " + nip);

    let sql = `SELECT p.pg_position_type, s.su_name, u.un_name, p.pg_full_name, p.pg_address, p.pg_blood_type, p.pg_nik, p.pg_first_name, p.pg_last_name, p.pg_first_degree, p.pg_last_degree,
    p.pg_email, p.pg_nip, p.pg_birth_place, p.pg_date_birthday, pa.pa_name, pa.pa_keputusan, pa.pa_date, gr.gr_name, gr.gr_group, 
    ga.ga_date, p.pg_religion, p.pg_marital_status, p.pg_gender, p.pg_num_of_child,
    p.pg_profile_pic, p.pg_paid_leave, p.pg_ktp, p.pg_kk, p.pg_npwp, p.pg_ijazah, p.pg_kartu_pegawai
    FROM t_pegawai p
    JOIN t_sub_unit s USING (su_id)
    JOIN t_unit u USING (un_id)
    JOIN t_position_assignment pa USING (pg_id)
    JOIN t_grade_assignment ga USING (pg_id)
    JOIN t_grade gr USING (gr_id)
    WHERE pg_nip = ?`;

    let data = await new Promise((resolve) =>
        connection.query(sql,
            [nip], function (error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                console.log(`AAAAAAA ${rows[0]} \n\n`);

                let user_data = {
                    name: rows[0].pg_full_name,
                    profile_pic: rows[0].pg_profile_pic,
                    ktp: rows[0].pg_ktp,
                    kk: rows[0].pg_kk,
                    npwp: rows[0].pg_npwp,
                    ijazah: rows[0].pg_ijazah,
                    kartu_pegawai: rows[0].pg_kartu_pegawai,
                    cuti: rows[0].pg_paid_leave,
                    full_name: rows[0].pg_full_name,
                    first_name: rows[0].pg_first_name,
                    last_name: rows[0].pg_last_name,
                    first_degree: rows[0].pg_first_degree,
                    last_degree: rows[0].pg_last_degree,
                    blood_type: rows[0].pg_blood_type,
                    address: rows[0].pg_address,
                    nik: rows[0].pg_nik,
                    nip: rows[0].pg_nip,
                    email: rows[0].pg_email,
                    role: rows[0].rl_id,
                    unit: rows[0].un_name,
                    sub_unit: rows[0].su_name,
                    birth_place: rows[0].pg_birth_place,
                    birth_date: rows[0].pg_date_birthday,
                    gender: rows[0].pg_gender,
                    religion: rows[0].pg_religion,
                    marital_status: rows[0].pg_marital_status,
                    num_of_child: rows[0].pg_num_of_child,
                    jabatan_nama: rows[0].pa_name,
                    jabatan_keputusan: rows[0].pa_keputusan,
                    jabatan_tanggal: rows[0].pa_date,
                    golongan_nama: rows[0].gr_name,
                    golongan_ruang: rows[0].gr_group,
                    golongan_tanggal: rows[0].ga_date,
                    position_type: rows[0].pg_position_type
                };

                return rows.length > 0 ? resolve(user_data) : resolve({});
            })
    );

    let sql_education = `SELECT ed_degree as degree, ed_major as major, ed_institution as institution, ed_year as year FROM t_education 
        JOIN t_pegawai p USING (pg_id) WHERE pg_nip = ?`;

    let data_education = await new Promise((resolve) =>
        connection.query(sql_education,
            [nip], function (error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                console.log("DATA PEGAWAI BY ID " + JSON.stringify(data) + "\n");
                console.log("DATA PEGAWAI BY ID " + data + "\n");
                console.log("DATA PEGAWAI BY ID " + data.jabatan_nama + "\n");

                return rows.length > 0 ? resolve(rows) : resolve({});
            })
    );

    let sql_diklat = `SELECT cf.cf_name as name, ct.ct_year as year FROM t_certification ct 
    JOIN t_certificate cf USING (cf_id) 
    JOIN t_pegawai p USING (pg_id) 
    WHERE pg_nip = ?`;

    let diklat_data = await new Promise((resolve) =>
        connection.query(sql_diklat,
            [nip],
            function(error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                return rows.length > 0 ? resolve(rows) : resolve({});
            })
    );

    let sql_reminder_promotion = `SELECT rm_is_remindered, rm_status_reminder FROM t_reminder WHERE pg_id = ? AND rm_type = "1"`;

    let reminder_data_promotion = await new Promise((resolve) =>
        connection.query(sql_reminder_promotion,
            [dataPgId],
            function(error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                return rows.length > 0 ? resolve(rows[0]) : resolve([]);
            })
    );

    let sql_reminder_retirement = `SELECT rm_is_remindered, rm_status_reminder FROM t_reminder WHERE pg_id = ? AND rm_type = "2"`;

    let reminder_data_retirement = await new Promise((resolve) =>
        connection.query(sql_reminder_retirement,
            [dataPgId],
            function(error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                return rows.length > 0 ? resolve(rows[0]) : resolve([]);
            })
    );
    
    let user_data = {
        name: data.name,
        picture: data.profile_pic,
        ktp: data.ktp,
        kk: data.kk,
        npwp: data.npwp,
        kartu_pegawai: data.kartu_pegawai,
        ijazah: data.ijazah,
        cuti: data.cuti,
        full_name: data.full_name,
        first_name: data.first_name,
        last_name: data.last_name,
        first_degree: data.first_degree,
        last_degree: data.last_degree,
        blood_type: data.blood_type,
        address: data.address,
        nik: data.nik,
        nip: data.nip,
        email: data.email,
        role: data.role,
        unit: data.unit,
        sub_unit: data.sub_unit,
        birth_place: data.birth_place,
        birth_date: data.birth_date,
        gender: data.gender,
        religion: data.religion,
        marital_status: data.marital_status,
        num_of_child: data.num_of_child,
        jabatan: {
            nama: data.jabatan_nama,
            keputusan: data.jabatan_keputusan,
            tanggal: data.jabatan_tanggal
        },
        golongan: {
            nama: data.golongan_nama,
            ruang: data.golongan_ruang,
            tanggal: data.golongan_tanggal
        },
        education: data_education,
        diklat: diklat_data,
        date_promotion: data.birth_date,
        date_retire: data.birth_date,
        position_type: data.position_type
    };

    return response.ok(user_data, '', reply);
}

async function editProfile(request, reply) {

    let now = moment().format('YYYY-MM-DD HH:mm:ss').toString();
    let updated_at = now;
    let token = request.headers.authorization;
    let check = await header.check(token, reply);

    let sql = `UPDATE t_pegawai set pg_full_name = ?, pg_first_name = ?, pg_last_name = ?, pg_first_degree = ?, 
    pg_last_degree = ?, pg_blood_type = ?, pg_address = ?, pg_nik = ?, pg_email = ?,
    pg_birth_place = ?, pg_date_birthday = ?, pg_religion = ?, pg_marital_status = ?, 
    pg_gender = ?, pg_num_of_child = ?, updated_at = ? WHERE pg_id = ?`;

    let pg_first_name = request.body.first_name;
    let pg_last_name = request.body.last_name;
    let pg_first_degree = request.body.first_degree;
    let pg_last_degree = request.body.last_degree;
    let pg_blood_type = request.body.blood_type;
    let pg_address = request.body.address;
    let pg_nik = request.body.nik;
    let pg_email = request.body.email;
    let pg_birth_place = request.body.birth_place;
    let pg_date_birthday = request.body.birth_date;
    let pg_religion = request.body.religion;
    let pg_marital_status = request.body.marital_status;
    let pg_num_of_child = request.body.num_of_child;
    let pg_gender = request.body.gender;
    let pg_full_name = `${pg_first_degree} ${pg_first_name} ${pg_last_name}, ${pg_last_degree}`;
    
    let data = await new Promise((resolve) =>
        connection.query(sql,
            [pg_full_name, pg_first_name, pg_last_name, pg_first_degree, pg_last_degree, pg_blood_type, pg_address, pg_nik, pg_email, 
                pg_birth_place, pg_date_birthday.substring(0, 10), pg_religion, pg_marital_status, 
                pg_gender, pg_num_of_child, updated_at, check.pg_id], 
                function (error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                console.log('AFFECTED UPDATE PROFILE ' + rows.affectedRows);

                return rows.affectedRows > 0 ? resolve(true) : resolve(false);
            })
    );

    let msg = data ? "Berhasil update profile!" : "Gagal update profile!";
    return response.ok(data, msg, reply);
}

async function updateProfileFiles(request, reply) {
    let now = moment().format('YYYY-MM-DD HH:mm:ss').toString();
    let now_pic = moment().format('YYYYMMDDHHmm').toString();
    let updated_at = now;
    let token = request.headers.authorization;
    let check = await header.check(token, reply);

    let sql_user = `SELECT pg_profile_pic, pg_ktp, pg_kk, pg_npwp, pg_kartu_pegawai, pg_ijazah
    FROM t_pegawai 
    WHERE pg_id = ?`;

    let user_data = await new Promise((resolve) =>
        connection.query(sql_user,
            [check.pg_id], function (error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                let user_data = {
                    picture: rows[0].pg_profile_pic,
                    ktp: rows[0].pg_ktp,
                    kk: rows[0].pg_kk,
                    npwp: rows[0].pg_npwp,
                    kartu_pegawai: rows[0].pg_kartu_pegawai,
                    ijazah: rows[0].pg_ijazah,
                };

                return rows.length > 0 ? resolve(user_data) : resolve({});
            })
    );

    let sql = `UPDATE t_pegawai set pg_profile_pic = ?, pg_ktp = ?, pg_kk = ?, pg_npwp = ?, 
    pg_kartu_pegawai = ?, pg_ijazah = ?, updated_at = ? WHERE pg_id = ?`;

    let pg_profile_pic = request.raw.files.picture;
    let filename_pic = user_data.picture;
    if (pg_profile_pic != null) {
        filename_pic = '/storage/' + 'pic-' + check.pg_id + '-' + now_pic + '-' + pg_profile_pic.name;
        pg_profile_pic.mv(path.join(__dirname, '../storage/') + 'pic-' + check.pg_id + '-' + now_pic + '-' + pg_profile_pic.name, function(err) {
            if (err)
                reply.send({ message: err, code: 500 });
            console.log('Uploaded! ' + pg_profile_pic.name);
        });
    }

    let pg_ktp = request.raw.files.ktp;
    let filename_ktp = user_data.ktp;
    if (pg_ktp != null) {
        filename_ktp = '/storage/' + 'ktp-' + check.pg_id + '-' + now_pic + '-' + pg_ktp.name;
        pg_ktp.mv(path.join(__dirname, '../storage/') + 'ktp-' + check.pg_id + '-' + now_pic + '-' + pg_ktp.name, function(err) {
            if (err)
                reply.send({ message: err, code: 500 });
            console.log('Uploaded! ' + pg_ktp.name);
        });

    }

    let pg_kk = request.raw.files.kk;
    let filename_kk = user_data.kk;
    if (pg_kk != null) {
        filename_kk = '/storage/' + 'kk-' + check.pg_id + '-' + now_pic + '-' + pg_kk.name;
        pg_kk.mv(path.join(__dirname, '../storage/') + 'kk-' + check.pg_id + '-' + now_pic + '-' + pg_kk.name, function(err) {
            if (err)
                reply.send({ message: err, code: 500 });
            console.log('Uploaded! ' + pg_kk.name);
        });
    }

    let pg_npwp = request.raw.files.npwp;
    let filename_npwp = user_data.npwp;
    if (pg_npwp != null) {
        filename_npwp = '/storage/' + 'npwp-' + check.pg_id + '-' + now_pic + '-' + pg_npwp.name;
        pg_npwp.mv(path.join(__dirname, '../storage/') + 'npwp-' + check.pg_id + '-' + now_pic + '-' + pg_npwp.name, function(err) {
            if (err)
                reply.send({ message: err, code: 500 });
            console.log('Uploaded! ' + pg_npwp.name);
        });
    }

    let pg_kartu_pegawai = request.raw.files.kartu_pegawai;
    let filename_kartu_pegawai = user_data.kartu_pegawai;
    if (pg_kartu_pegawai != null) {
        filename_kartu_pegawai = '/storage/' + 'kartu_pegawai-' + check.pg_id + '-' + now_pic + '-' + pg_kartu_pegawai.name;
        pg_kartu_pegawai.mv(path.join(__dirname, '../storage/') + 'kartu_pegawai-' + check.pg_id + '-' + now_pic + '-' + pg_kartu_pegawai.name, function(err) {
            if (err)
                reply.send({ message: err, code: 500 });
            console.log('Uploaded! ' + pg_kartu_pegawai.name);
        });
    }

    let pg_ijazah = request.raw.files.ijazah;
    let filename_ijazah = user_data.ijazah;
    if (pg_ijazah != null) {
        filename_ijazah = '/storage/' + 'ijazah-' + check.pg_id + '-' + now_pic + '-' + pg_ijazah.name;
        pg_ijazah.mv(path.join(__dirname, '../storage/') + 'ijazah-' + check.pg_id + '-' + now_pic + '-' + pg_ijazah.name, function(err) {
            if (err)
                reply.send({ message: err, code: 500 });
            console.log('Uploaded! ' + pg_ijazah.name);
        });
    }
    
    let data = await new Promise((resolve) =>
        connection.query(sql,
            [filename_pic, filename_ktp, filename_kk, filename_npwp, filename_kartu_pegawai, filename_ijazah,
                updated_at, check.pg_id], 
                function (error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                console.log('AFFECTED UPDATE PROFILE ' + rows.affectedRows);

                return rows.affectedRows > 0 ? resolve(true) : resolve(false);
            })
    );

    let msg = data ? "Berhasil update files profile!" : "Gagal update files profile!";
    return response.ok(data, msg, reply);
}

module.exports = {
    getMyProfile, editProfile, getProfileByNip, updateProfileFiles
};