let response = require('../response');
let connection = require('../connection');
let sha1 = require('sha1');
let moment = require('moment');
let crypto = require('crypto');
let header = require('../helpers/token');
let path = require('path');

async function updateDocuments(request, reply) {

    let now = moment().format('YYYY-MM-DD HH:mm:ss').toString();
    let now_pic = moment().format('YYYYMMDDHHmm').toString();
    let updated_at = now;
    let token = request.headers.authorization;

    let check = await header.check(token, reply);

    let data_file_1 = null;
    let data_file_2 = null;
    let data_file_3 = null;

    // File 1 (SK CPNS)]
    let file_1 = request.raw.files.skcpns;
    if (file_1 != null) {
        let sql_file_1 = `UPDATE t_documents set dc_url = ?, updated_at = ? WHERE pg_id = ? AND dc_type = 1`;
    
        let filename_1 = '/storage/' + 'sk-cpns-' + check.pg_id + '-' + now_pic + '-' + file_1.name;
        file_1.mv(path.join(__dirname, '../storage/') + 'sk-cpns-' + check.pg_id + '-' + now_pic + '-' + file_1.name, function(err) {
            if (err)
                reply.send({ message: err, code: 500 });
            console.log('Uploaded! ' + file_1.name);
        });
    
        data_file_1 = await new Promise((resolve) =>
            connection.query(sql_file_1,
                [filename_1, now, check.pg_id], 
                    function (error, rows) {
                    if (error) {
                        console.log(error);
                        return response.badRequest('', `${error}`, reply)
                    }
    
                    console.log('AFFECTED UPDATE DOCUMENTS SK CPNS ' + rows.affectedRows);
    
                    return rows.affectedRows > 0 ? resolve(true) : resolve(false);
                })
        );
    }
    
    let file_2 = request.raw.files.skpns;
    if (file_2 != null) {
        // File 2 (SK PNS)
        let sql_file_2 = `UPDATE t_documents set dc_url = ?, updated_at = ? WHERE pg_id = ? AND dc_type = 2`;
    
        let filename_2 = '/storage/' + 'sk-pns-' + check.pg_id + '-' + now_pic + '-' + file_2.name;
        file_2.mv(path.join(__dirname, '../storage/') + 'sk-pns-' + check.pg_id + '-' + now_pic + '-' + file_2.name, function(err) {
            if (err)
                reply.send({ message: err, code: 500 });
            console.log('Uploaded! ' + file_2.name);
        });
    
        data_file_2 = await new Promise((resolve) =>
            connection.query(sql_file_2,
                [filename_2, now, check.pg_id], 
                    function (error, rows) {
                    if (error) {
                        console.log(error);
                        return response.badRequest('', `${error}`, reply)
                    }
    
                    console.log('AFFECTED UPDATE DOCUMENTS SK PNS ' + rows.affectedRows);
    
                    return rows.affectedRows > 0 ? resolve(true) : resolve(false);
                })
        );
    }

    let file_3 = request.raw.files.skp;
    if (file_3 != null) {
        // File 3 (SKP)
        let sql_file_3 = `UPDATE t_documents set dc_url = ?, updated_at = ? WHERE pg_id = ? AND dc_type = 3`;

        let filename_3 = '/storage/' + 'sk-p-' + check.pg_id + '-' + now_pic + '-' + file_3.name;
        file_3.mv(path.join(__dirname, '../storage/') + 'sk-p-' + check.pg_id + '-' + now_pic + '-' + file_3.name, function(err) {
            if (err)
                reply.send({ message: err, code: 500 });
            console.log('Uploaded! ' + file_3.name);
        });

        data_file_3 = await new Promise((resolve) =>
            connection.query(sql_file_3,
                [filename_3, now, check.pg_id], 
                    function (error, rows) {
                    if (error) {
                        console.log(error);
                        return response.badRequest('', `${error}`, reply);
                    }

                    console.log('AFFECTED UPDATE DOCUMENTS SKP ' + rows.affectedRows);

                    return rows.affectedRows > 0 ? resolve(true) : resolve(false);
                })
        );
    }
    

    let msg = data_file_1 || data_file_2 || data_file_3 ? "Berhasil update documents!" : "Gagal update documents!";
    return response.ok('Update Mantul', msg, reply);
}

async function getDocumentsByNip(request, reply) {
    let nip = request.query.nip;

    let sqlPgId = `SELECT pg_id FROM t_pegawai WHERE pg_nip = ?`;

    let dataPgId = await new Promise((resolve) =>
        connection.query(sqlPgId,
            [nip],
            function(error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                return rows.length > 0 ? resolve(rows[0].pg_id) : resolve('');
            })
    );

    console.log("ASDJASLKDJLAKSJDLKASD " + nip);
    let sql = `SELECT dc_type, dc_url
    FROM t_documents
    WHERE pg_id = ?`;

    let data = await new Promise((resolve) =>
        connection.query(sql,
            [dataPgId], function (error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                if (rows.length > 0) {
                    let documents_data = [];
                    rows.forEach(doc => {
                        documents_data.push({
                            type: doc.dc_type,
                            url: doc.dc_url,
                            ext: doc.dc_url.split(".")[1]
                        });
                    });

                    return resolve(documents_data);
                } else {
                    return resolve([]);
                }
            })
    );

    let docs = {
        nip: nip,
        documents: data,
    };

    return response.ok(docs, '', reply);
}

module.exports = {
    updateDocuments, getDocumentsByNip
};