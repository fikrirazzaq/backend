let response = require('../response');
let connection = require('../connection');
let sha1 = require('sha1');
let moment = require('moment');
let crypto = require('crypto');
let header = require('../helpers/token');

var MailConfig = require('../config/email');
var hbs = require('nodemailer-express-handlebars');
var gmailTransport = MailConfig.GmailTransport;

async function getUpcomingPromotion(request, reply) {

    let sql = `SELECT s.su_id, s.su_name, u.un_id, u.un_name, p.pg_id, p.pg_email, p.pg_full_name, p.pg_nip, p.pg_position_type, p.pg_date_retire, p.pg_date_promotion_next, p.pg_birth_place, p.pg_date_birthday, pa.pa_name, pa.pa_keputusan, pa.pa_date, gr.gr_name, gr.gr_group, ga.ga_date, p.pg_religion, p.pg_marital_status, p.pg_gender, p.pg_num_of_child, rm.rm_is_remindered, rm.rm_status_reminder
    FROM t_pegawai p
    LEFT JOIN t_sub_unit s ON p.su_id = s.su_id
    LEFT JOIN t_unit u ON s.un_id = u.un_id
    LEFT JOIN t_position_assignment pa ON p.pg_id = pa.pg_id
    LEFT JOIN t_grade_assignment ga ON p.pg_id = ga.pg_id
    LEFT JOIN t_grade gr ON ga.gr_id = gr.gr_id
    LEFT JOIN t_reminder rm ON rm.pg_id = p.pg_id
    WHERE rm.rm_type = "1" AND rm.rm_is_remindered = 0 AND rm.rm_status_reminder <> 3
    UNION
    SELECT s.su_id, s.su_name, u.un_id, u.un_name, p.pg_id, p.pg_email, p.pg_full_name, p.pg_nip, p.pg_position_type, p.pg_date_retire, p.pg_date_promotion_next, p.pg_birth_place, p.pg_date_birthday, pa.pa_name, pa.pa_keputusan, pa.pa_date, gr.gr_name, gr.gr_group, ga.ga_date, p.pg_religion, p.pg_marital_status, p.pg_gender, p.pg_num_of_child, rm.rm_is_remindered, rm.rm_status_reminder
    FROM t_pegawai p
    RIGHT JOIN t_sub_unit s ON p.su_id = s.su_id
    RIGHT JOIN t_unit u ON s.un_id = u.un_id
    RIGHT JOIN t_position_assignment pa ON p.pg_id = pa.pg_id
    RIGHT JOIN t_grade_assignment ga ON p.pg_id = ga.pg_id
    RIGHT JOIN t_grade gr ON ga.gr_id = gr.gr_id
    RIGHT JOIN t_reminder rm ON rm.pg_id = p.pg_id
    WHERE rm.rm_type = "1" AND rm.rm_is_remindered = 0 AND rm.rm_status_reminder <> 3`;

    let data = await new Promise((resolve) => connection.query(sql, [], function(error, rows) {
        if (error) {
            console.log(error);
            return response.badRequest('', `${error}`, reply)
        }

        if (rows.length > 0) {
            let array = fetchAllEmployees(rows);
            return resolve(array);
        } else {
            return resolve([]);
        }
    }));

    return response.ok(data, '', reply);
}

async function fetchAllEmployees(rows) {
    let array = []
    for(let element of rows) {
        try {
            let sqlSchool = `SELECT ed_degree, ed_major, ed_institution, ed_year FROM t_education WHERE pg_id = ?`;

            let schoolData = await new Promise((resolve) =>
                connection.query(sqlSchool,
                    [element.pg_id],
                    function(error, rows) {
                        if (error) {
                            console.log(error);
                            return response.badRequest('', `${error}`, reply)
                        }

                        if (rows.length > 0) {
                            let school_data = [];
                            rows.forEach(school => {
                                school_data.push({
                                    degree: school.ed_degree,
                                    major: school.ed_major,
                                    institution: school.ed_institution,
                                    year: school.ed_year,
                                });
                            });

                            return resolve(school_data);
                        } else {
                            return resolve([]);
                        }
                    })
            );

            let sqlDiklat = `SELECT cf.cf_name, ct.ct_year FROM t_certification ct JOIN t_certificate cf USING (cf_id) WHERE pg_id = ?`;

            let diklatData = await new Promise((resolve) =>
                connection.query(sqlDiklat,
                    [element.pg_id],
                    function(error, rows) {
                        if (error) {
                            console.log(error);
                            return response.badRequest('', `${error}`, reply)
                        }

                        if (rows.length > 0) {
                            let diklat_data = [];
                            rows.forEach(diklat => {
                                diklat_data.push({
                                    name: diklat.cf_name,
                                    year: diklat.ct_year,
                                });
                            });

                            return resolve(diklat_data);
                        } else {
                            return resolve([]);
                        }
                    })
            );

            if (element.pg_nip != null) {
                array.push({
                    name: element.pg_full_name,
                    nip: element.pg_nip,
                    email: element.pg_email,
                    role: element.rl_id,
                    unit: element.un_name,
                    birth_place: element.pg_birth_place,
                    birth_date: element.pg_date_birthday,
                    gender: element.pg_gender,
                    religion: element.pg_religion,
                    marital_status: element.pg_marital_status,
                    num_of_child: element.pg_num_of_child,
                    jabatan: {
                        nama: element.pa_name,
                        keputusan: element.pa_keputusan,
                        tanggal: element.pa_date
                    },
                    golongan: {
                        nama: element.gr_name,
                        ruang: element.gr_group,
                        tanggal: element.ga_date
                    },
                    education: schoolData,
                    diklat: diklatData,
                    position_type: element.pg_position_type,
                    reminder: {
                        is_remindered: element.rm_is_remindered,
                        status_reminder: element.rm_status_reminder,
                        date_next_promotion: element.pg_date_promotion_next,
                    }
                });
            }

        } catch (error) {
            console.log('error'+ error);
        }
    }
    return array
}


module.exports = {
    getUpcomingPromotion
};