let response = require('../response');
let connection = require('../connection');
let sha1 = require('sha1');
let moment = require('moment');
let crypto = require('crypto');
let header = require('../helpers/token');

var MailConfig = require('../config/email');
var hbs = require('nodemailer-express-handlebars');
var gmailTransport = MailConfig.GmailTransport;

async function setYoutubeUrl(request, reply) {
    let now = moment().format('YYYY-MM-DD HH:mm:ss').toString();
    let sql = `UPDATE t_youtube_url SET url= ?, updated_at = ? WHERE id = 1`;

    let data = await new Promise((resolve) =>
        connection.query(sql,
            [request.body.url, now], function (error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                console.log("AFFECTED " + rows.affectedRows);

                return rows.affectedRows > 0 ? resolve(true) : resolve(false);
            })
    );

    let msg = data ? "Youtube url updated!" : "Youtube url update failed!";
    return response.ok(data, msg, reply);
}

async function getYoutubeUrl(request, reply) {
    let sql = `SELECT url FROM t_youtube_url WHERE id = 1`;

    let data = await new Promise((resolve) => connection.query(sql, [], function (error, rows) {
        if (error) {
            console.log(error);
            return response.badRequest('', `${error}`, reply)
        }

        if (rows.length > 0) {

            let url = {
                url: rows[0].url
            }

            return resolve(url);
        }
        else {
            return resolve({});
        }
    })
    );

    return response.ok(data, '', reply);
}

async function getRunningText(request, reply) {
    let sql = `SELECT rt_id, rt_content, created_at, updated_at FROM t_running_text WHERE rt_id = 1`;

    let data = await new Promise((resolve) => connection.query(sql, [], function (error, rows) {
        if (error) {
            console.log(error);
            return response.badRequest('', `${error}`, reply)
        }

        if (rows.length > 0) {

            let running_text = {
                id_running_text: rows[0].rt_id,
                running_text: rows[0].rt_content,
                created_at: rows[0].created_at,
                updated_at: rows[0].updated_at
            }

            return resolve(running_text);
        }
        else {
            return resolve([]);
        }
    })
    );

    return response.ok(data, '', reply);
}

async function addRunningText(request, reply) {
    let now = moment().format('YYYY-MM-DD HH:mm:ss').toString();

    let sql = `INSERT INTO t_running_text(rt_content, created_at, updated_at) VALUES (?, ?, ?)`;

    let data = await new Promise((resolve) =>
        connection.query(sql,
            [request.body.running_text, now, now], function (error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                console.log("New running text inserted " + rows.affectedRows);

                return rows.affectedRows > 0 ? resolve(true) : resolve(false);
            })
    );

    let msg = data ? "Running text created!" : "Create running text failed!";
    return response.ok(data, msg, reply);
}

async function deleteRunningText(request, reply) {
    let sql = `DELETE FROM t_running_text WHERE rt_id = ?`;

    let data = await new Promise((resolve) =>
        connection.query(sql,
            [request.body.id_running_text], function (error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                console.log("Running text deleted " + rows.affectedRows);

                return rows.affectedRows > 0 ? resolve(true) : resolve(false);
            })
    );

    let msg = data ? "Running text deleted!" : "Delete running text failed!";
    return response.ok(data, msg, reply);
}

async function updateRunningText(request, reply) {
    let now = moment().format('YYYY-MM-DD HH:mm:ss').toString();

    let sql = `UPDATE t_running_text SET rt_content = ?, updated_at = ? WHERE rt_id = 1`;

    let data = await new Promise((resolve) =>
        connection.query(sql,
            [request.body.running_text, now], function (error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                console.log("Running text updated " + rows.affectedRows);

                return rows.affectedRows > 0 ? resolve(true) : resolve(false);
            })
    );

    let msg = data ? "Running text updated!" : "Update running text failed!";
    return response.ok(data, msg, reply);
}

async function addFlashNews(request, reply) {
    let now = moment().format('YYYY-MM-DD HH:mm:ss').toString();

    let sql = `INSERT INTO t_news(nw_title, nw_image, nw_content, created_at, updated_at) VALUES (?, ?, ?, ?, ?)`;

    let data = await new Promise((resolve) =>
        connection.query(sql,
            [request.body.title, request.body.image_url, request.body.content, now, now], function (error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                console.log("New flash news inserted " + rows.affectedRows);

                return rows.affectedRows > 0 ? resolve(true) : resolve(false);
            })
    );

    let msg = data ? "Flash news created!" : "Create flash news failed!";
    return response.ok(data, msg, reply);
}

async function updateFlashNews(request, reply) {
    let now = moment().format('YYYY-MM-DD HH:mm:ss').toString();

    let sql = `UPDATE t_news SET nw_title = ? , nw_image = ?, nw_content = ?, updated_at = ? WHERE nw_id = ?`;

    let data = await new Promise((resolve) =>
        connection.query(sql,
            [request.body.title, request.body.image_url, request.body.content, now, request.body.id_news], function (error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                console.log("New flash news updated " + rows.affectedRows);

                return rows.affectedRows > 0 ? resolve(true) : resolve(false);
            })
    );

    let msg = data ? "Flash news updated!" : "Create flash news updated!";
    return response.ok(data, msg, reply);
}

async function deleteFlashNews(request, reply) {
    let now = moment().format('YYYY-MM-DD HH:mm:ss').toString();

    let sql = `DELETE FROM t_news WHERE nw_id = ?`;

    let data = await new Promise((resolve) =>
        connection.query(sql,
            [request.body.id_news], function (error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                console.log("New flash news deleted " + rows.affectedRows);

                return rows.affectedRows > 0 ? resolve(true) : resolve(false);
            })
    );

    let msg = data ? "Flash news deleted!" : "Delete flash news failed!";
    return response.ok(data, msg, reply);
}

async function getFlashNews(request, reply) {
    let sql = `SELECT nw_id, nw_title, nw_image, nw_content, created_at, updated_at FROM t_news`;

    let data = await new Promise((resolve) => connection.query(sql, [], function (error, rows) {
        if (error) {
            console.log(error);
            return response.badRequest('', `${error}`, reply)
        }

        if (rows.length > 0) {
            let array = [];
            rows.forEach(element => {
                array.push({
                    id_news: element.nw_id,
                    title: element.nw_title,
                    content: element.nw_content,
                    image_url: element.nw_image,
                    created_at: element.created_at,
                    updated_at: element.updated_at
                });
            });

            return resolve(array);
        }
        else {
            return resolve([]);
        }
    })
    );

    return response.ok(data, '', reply);
}

module.exports = {
    getYoutubeUrl, setYoutubeUrl, addRunningText, deleteRunningText, updateRunningText, getRunningText, addFlashNews, deleteFlashNews, updateFlashNews, getFlashNews
};