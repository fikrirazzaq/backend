let response = require('../response');
let connection = require('../connection');
let sha1 = require('sha1');
let moment = require('moment');
let crypto = require('crypto');
let header = require('../helpers/token');

var MailConfig = require('../config/email_reminder_promotion_admin');
var hbs = require('nodemailer-express-handlebars');
var gmailTransport = MailConfig.GmailTransport;

async function sendReminder(request, reply) {
    let now = moment().format('YYYY-MM-DD HH:mm:ss').toString();

    let pg_nip = request.body.nip;
    let np_type = request.body.type;

    let sqlPgId = `SELECT pg_id, pg_email, pg_full_name FROM t_pegawai WHERE pg_nip = ?`;

    let dataPgId = await new Promise((resolve) =>
        connection.query(sqlPgId,
            [pg_nip],
            function(error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                return rows.length > 0 ? resolve(rows[0]) : resolve('');
            })
    );

    // Insert Notif to Notif Pegawai
    var np_name = "";
    var np_content = "";
    switch(np_type) {
        case "1":
            np_name = "Promosi";
            np_content = "Silahkan lengkapi persyaratan untuk kenaikan pangkat.";            
            break;
        case "2":
            np_name = "Pensiun";
            np_content = "Silahkan lengkapi persyaratan untuk kenaikan pangkat.";
            break;
        case "3":
            np_name = "Ulang Tahun";
            np_content = "Atep ulang tahun hari ini.";
            break;
        default:
            np_name = "Promosi";
            np_content = "Silahkan lengkapi persyaratan untuk kenaikan pangkat.";
            break;
    }

    // Send Notif to Email Pegawai

    MailConfig.ViewOption(gmailTransport,hbs);
    let HelperOptions = {
        from: '"Kemnaker TU" <kemnaker.tu@gmail.com>',
        to: dataPgId.pg_email,
        subject: np_name,
        template: 'email',
        context: {
            nama_pegawai: "Reminder " + np_name,
        }
    };
    gmailTransport.sendMail(HelperOptions, (error,info) => {
        if(error) {
        console.log(error);
        reply.send({ message: error, code: 400 });
        }
        console.log("email is send");
        console.log(info);
        reply.send({code: 200, message: 'Email sent!', values: info});
    });

    let sqlInsertNoitf = `INSERT INTO t_notif_pegawai 
            (np_type, np_name, np_content, np_status_read, np_time_received, pg_id) 
            VALUES (?, ?, ?, '0', ?, ?)`;
    let dataInsert = await new Promise((resolve) =>
        connection.query(sqlInsertNoitf,
            [np_type, np_name, np_content, now, dataPgId.pg_id], function (error, rows) {
                if (error) {
                    console.log(error);
                    if (`${error}`.includes("np_time_received")) {
                        return response.ok(true, "Reminder sent!", reply);
                    }
                    return response.badRequest('', `${error}`, reply)
                }

                console.log("Insert notif employee success " + rows.affectedRows);

                return rows.affectedRows > 0 ? resolve(true) : resolve(false);
            })
    );

    let msg = dataInsert? "Reminder sent!" : "Failed to send reminder!";
    return response.ok(dataInsert, msg, reply);
}

async function sendValidNotif(request, reply) {
    let now = moment().format('YYYY-MM-DD HH:mm:ss').toString();

    let pg_nip = request.body.nip;
    let np_type = request.body.type;
    let ga_date = request.body.date_promotion;

    let sqlPgId = `SELECT pg_id, pg_email, pg_full_name FROM t_pegawai WHERE pg_nip = ?`;

    let dataPgId = await new Promise((resolve) =>
        connection.query(sqlPgId,
            [pg_nip],
            function(error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                return rows.length > 0 ? resolve(rows[0]) : resolve('');
            })
    );

    // Insert Notif to Notif Pegawai
    var np_name = "";
    var np_content = "";
    switch(np_type) {
        case "1":
            np_name = "Promosi";
            np_content = "Proses kenaikan pangkat berhasil.";            
            break;
        case "2":
            np_name = "Pensiun";
            np_content = "Proses pensiun berhasil.";
            break;
        case "3":
            np_name = "Ulang Tahun";
            np_content = "Atep ulang tahun hari ini.";
            break;
        default:
            np_name = "Promosi";
            np_content = "Proses kenaikan pangkat berhasil.";
            break;
    }

    // Update t_grade_assignment
    let sql_get_current_gol = `SELECT gr_id FROM t_grade_assignment WHERE pg_id = ?`;
    let currentGol = await new Promise((resolve) =>
        connection.query(sql_get_current_gol,
            [dataPgId.pg_id],
            function(error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                return rows.length > 0 ? resolve(rows[0].gr_id) : resolve('');
            })
    );

    let nextGol = currentGol + 1;

    let sql_update_gol = `UPDATE t_grade_assignment SET ga_date = ?, gr_id = ?, updated_at = ? 
            WHERE pg_id = ?`;
    let data_update_gol = await new Promise((resolve) =>
        connection.query(sql_update_gol,
            [ga_date, nextGol, now, dataPgId.pg_id], function (error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                console.log("Update Gol Success Affected: " + rows.affectedRows);

                return rows.affectedRows > 0 ? resolve(true) : resolve(false);
            })
    );

    // Send Notif to Email Pegawai

    MailConfig.ViewOption(gmailTransport,hbs);
    let HelperOptions = {
        from: '"Kemnaker TU" <kemnaker.tu@gmail.com>',
        to: dataPgId.pg_email,
        subject: np_name,
        template: 'email',
        context: {
            title: "Reminder " + np_name,
            name: dataPgId.pg_full_name,
            email: dataPgId.pg_email,
            address: np_content
        }
    };
    gmailTransport.sendMail(HelperOptions, (error,info) => {
        if(error) {
        console.log(error);
        reply.send({ message: error, code: 400 });
        }
        console.log("email is send");
        console.log(info);
        reply.send({code: 200, message: 'Email sent!', values: info});
    });

    let sqlInsertNoitf = `INSERT INTO t_notif_pegawai 
        (np_type, np_name, np_content, np_status_read, np_time_received, pg_id) 
        VALUES (?, ?, ?, '0', ?, ?)`;
    let dataInsert = await new Promise((resolve) =>
        connection.query(sqlInsertNoitf,
            [np_type, np_name, np_content, now, dataPgId.pg_id], function (error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                console.log("Insert notif employee success " + rows.affectedRows);

                return rows.affectedRows > 0 ? resolve(true) : resolve(false);
            })
    );

    let msg = dataInsert && data_update_gol? "Validasi status updated!" : "Validasi status update failed!";
    return response.ok(dataInsert, msg, reply);
}

module.exports = {
    sendReminder, sendValidNotif
};