let response = require('../response');
let connection = require('../connection');
let sha1 = require('sha1');
let moment = require('moment');
let crypto = require('crypto');
let header = require('../helpers/token');

var MailConfig = require('../config/email');
var hbs = require('nodemailer-express-handlebars');
var gmailTransport = MailConfig.GmailTransport;

async function getAllEmployee(request, reply) {

    let sql = `SELECT p.rl_id, s.su_name, u.un_name, p.pg_ktp, p.pg_kk, p.pg_kartu_pegawai, p.pg_npwp, p.pg_ijazah, p.pg_id, p.pg_first_degree, p.pg_last_degree, p.pg_blood_type, p.pg_nik, p.pg_address, p.pg_email, p.pg_full_name, p.pg_first_name, p.pg_last_name, p.pg_nip, p.pg_position_type, p.pg_date_retire, p.pg_date_promotion_next, p.pg_birth_place, p.pg_date_birthday, pa.pa_name, pa.pa_keputusan, pa.pa_date, gr.gr_name, gr.gr_group, ga.ga_date, p.pg_religion, p.pg_marital_status, p.pg_gender, p.pg_num_of_child, p.pg_profile_pic, p.pg_paid_leave
    FROM t_pegawai p
    LEFT JOIN t_sub_unit s ON p.su_id = s.su_id
    LEFT JOIN t_unit u ON s.un_id = u.un_id
    LEFT JOIN t_position_assignment pa ON p.pg_id = pa.pg_id
    LEFT JOIN t_grade_assignment ga ON p.pg_id = ga.pg_id
    LEFT JOIN t_grade gr ON ga.gr_id = gr.gr_id
    UNION
    SELECT p.rl_id, s.su_name, u.un_name, p.pg_ktp, p.pg_kk, p.pg_kartu_pegawai, p.pg_npwp, p.pg_ijazah, p.pg_id, p.pg_first_degree, p.pg_last_degree, p.pg_blood_type, p.pg_nik, p.pg_address, p.pg_email, p.pg_full_name, p.pg_first_name, p.pg_last_name, p.pg_nip, p.pg_position_type, p.pg_date_retire, p.pg_date_promotion_next, p.pg_birth_place, p.pg_date_birthday, pa.pa_name, pa.pa_keputusan, pa.pa_date, gr.gr_name, gr.gr_group, ga.ga_date, p.pg_religion, p.pg_marital_status, p.pg_gender, p.pg_num_of_child, p.pg_profile_pic, p.pg_paid_leave
    FROM t_pegawai p
    RIGHT JOIN t_sub_unit s ON p.su_id = s.su_id
    RIGHT JOIN t_unit u ON s.un_id = u.un_id
    RIGHT JOIN t_position_assignment pa ON p.pg_id = pa.pg_id
    RIGHT JOIN t_grade_assignment ga ON p.pg_id = ga.pg_id
    RIGHT JOIN t_grade gr ON ga.gr_id = gr.gr_id
    ORDER BY un_name DESC, su_name DESC`;

    let data = await new Promise((resolve) => connection.query(sql, [], function(error, rows) {
        if (error) {
            console.log(error);
            return response.badRequest('', `${error}`, reply)
        }

        if (rows.length > 0) {
            let array = fetchAllEmployees(rows);
            return resolve(array);
        } else {
            return resolve([]);
        }
    }));

    return response.ok(data, '', reply);
}

async function fetchAllEmployees(rows) {
    let array = []
    for(let element of rows) {
        try {
            let sqlSchool = `SELECT ed_degree, ed_major, ed_institution, ed_year FROM t_education WHERE pg_id = ?`;

            let schoolData = await new Promise((resolve) =>
                connection.query(sqlSchool,
                    [element.pg_id],
                    function(error, rows) {
                        if (error) {
                            console.log(error);
                            return response.badRequest('', `${error}`, reply)
                        }

                        if (rows.length > 0) {
                            let school_data = [];
                            rows.forEach(school => {
                                school_data.push({
                                    degree: school.ed_degree,
                                    major: school.ed_major,
                                    institution: school.ed_institution,
                                    year: school.ed_year,
                                });
                            });

                            return resolve(school_data);
                        } else {
                            return resolve([]);
                        }
                    })
            );

            let sqlDiklat = `SELECT cf.cf_name, ct.ct_year FROM t_certification ct JOIN t_certificate cf USING (cf_id) WHERE pg_id = ?`;

            let diklatData = await new Promise((resolve) =>
                connection.query(sqlDiklat,
                    [element.pg_id],
                    function(error, rows) {
                        if (error) {
                            console.log(error);
                            return response.badRequest('', `${error}`, reply)
                        }

                        if (rows.length > 0) {
                            let diklat_data = [];
                            rows.forEach(diklat => {
                                diklat_data.push({
                                    name: diklat.cf_name,
                                    year: diklat.ct_year,
                                });
                            });

                            return resolve(diklat_data);
                        } else {
                            return resolve([]);
                        }
                    })
            );

            if (element.pg_nip != null) {
                array.push({
                    unit: element.un_name,
                    sub_unit: element.su_name,
                    name: element.pg_full_name,
                    picture: element.pg_profile_pic,
                    cuti: element.pg_paid_leave,
                    ktp: element.pg_ktp,
                    kk: element.pg_kk,
                    npwp: element.pg_npwp,
                    kartu_pegawai: element.pg_kartu_pegawai,
                    ijazah: element.pg_ijazah,
                    first_name: element.pg_first_name,
                    last_name: element.pg_last_name,
                    first_degree: element.pg_first_degree,
                    last_degree: element.pg_last_degree,
                    blood_type: element.pg_blood_type,
                    address: element.pg_address,
                    nik: element.pg_nik,
                    nip: element.pg_nip,
                    email: element.pg_email,
                    role: element.rl_id,
                    birth_place: element.pg_birth_place,
                    birth_date: element.pg_date_birthday,
                    gender: element.pg_gender,
                    religion: element.pg_religion,
                    marital_status: element.pg_marital_status,
                    num_of_child: element.pg_num_of_child,
                    jabatan: {
                        nama: element.pa_name,
                        keputusan: element.pa_keputusan,
                        tanggal: element.pa_date
                    },
                    golongan: {
                        nama: element.gr_name,
                        ruang: element.gr_group,
                        tanggal: element.ga_date
                    },
                    education: schoolData,
                    diklat: diklatData,
                    date_next_promotion: element.pg_date_promotion_next,
                    date_retire: element.pg_date_retire,
                    position_type: element.pg_position_type
                });
            }

        } catch (error) {
            console.log('error'+ error);
        }
    }
    return array
}

async function getEmployeeByNip(request, reply) {

    if (request.query.nip == null || request.query.nip == "") {
        return response.badRequest('Empty NIP', `NIP is Empty`, reply)
    }

    let nip = request.query.nip;
    console.log("ASDJASLKDJLAKSJDLKASD " + nip);
    let token = request.headers.authorization;

    let sqlPgId = `SELECT pg_id FROM t_pegawai WHERE pg_nip = ?`;

    let dataPgId = await new Promise((resolve) =>
        connection.query(sqlPgId,
            [nip],
            function(error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                return rows.length > 0 ? resolve(rows[0].pg_id) : resolve('');
            })
    );

    console.log('PG ID ' + dataPgId);

    let sql = `SELECT p.rl_id, s.su_name, u.un_name, p.pg_ktp, p.pg_kk, p.pg_kartu_pegawai, p.pg_npwp, p.pg_ijazah, p.pg_full_name, p.pg_first_name, p.pg_last_name, p.pg_first_degree, p.pg_last_degree, p.pg_blood_type, p.pg_nik, p.pg_address, p.pg_email, p.pg_nip, p.pg_position_type, p.pg_date_retire, p.pg_date_promotion_next, p.pg_birth_place, p.pg_date_birthday, pa.pa_name, pa.pa_keputusan, pa.pa_date, gr.gr_name, gr.gr_group, ga.ga_date, p.pg_religion, p.pg_marital_status, p.pg_gender, p.pg_num_of_child, p.pg_profile_pic, p.pg_paid_leave
    FROM t_pegawai p
    JOIN t_sub_unit s USING (su_id)
    JOIN t_unit u USING (un_id)
    JOIN t_position_assignment pa USING (pg_id)
    JOIN t_grade_assignment ga USING (pg_id)
    JOIN t_grade gr USING (gr_id)
    WHERE pg_nip = ?`;

    let data = await new Promise((resolve) =>
        connection.query(sql,
            [nip], function (error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                let user_data = {
                    name: rows[0].pg_full_name,
                    picture: rows[0].pg_profile_pic,
                    cuti: rows[0].pg_paid_leave,
                    ktp: rows[0].pg_ktp,
                    kk: rows[0].pg_kk,
                    npwp: rows[0].pg_npwp,
                    kartu_pegawai: rows[0].pg_kartu_pegawai,
                    ijazah: rows[0].pg_ijazah,
                    first_name: rows[0].pg_first_name,
                    last_name: rows[0].pg_last_name,
                    first_degree: rows[0].pg_first_degree,
                    last_degree: rows[0].pg_last_degree,
                    blood_type: rows[0].pg_blood_type,
                    address: rows[0].pg_address,
                    nik: rows[0].pg_nik,
                    nip: rows[0].pg_nip,
                    email: rows[0].pg_email,
                    role: rows[0].rl_id,
                    unit: rows[0].un_name,
                    sub_unit: rows[0].su_name,
                    birth_place: rows[0].pg_birth_place,
                    birth_date: rows[0].pg_date_birthday,
                    gender: rows[0].pg_gender,
                    religion: rows[0].pg_religion,
                    marital_status: rows[0].pg_marital_status,
                    num_of_child: rows[0].pg_num_of_child,
                    jabatan_nama: rows[0].pa_name,
                    jabatan_keputusan: rows[0].pa_keputusan,
                    jabatan_tanggal: rows[0].pa_date,
                    golongan_nama: rows[0].gr_name,
                    golongan_ruang: rows[0].gr_group,
                    golongan_tanggal: rows[0].ga_date,
                    position_type: rows[0].pg_position_type
                };

                return rows.length > 0 ? resolve(user_data) : resolve({});
            })
    );

    let sql_education = `SELECT ed_degree as degree, ed_major as major, ed_institution as institution, ed_year as year FROM t_education 
        JOIN t_pegawai p USING (pg_id) WHERE pg_id = ?`;

    let data_education = await new Promise((resolve) =>
        connection.query(sql_education,
            [dataPgId], function (error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                console.log("DATA PEGAWAI BY ID " + JSON.stringify(data) + "\n");
                console.log("DATA PEGAWAI BY ID " + data + "\n");
                console.log("DATA PEGAWAI BY ID " + data.jabatan_nama + "\n");

                return rows.length > 0 ? resolve(rows) : resolve([]);
            })
    );

    let sql_diklat = `SELECT cf.cf_name as name, ct.ct_year as year FROM t_certification ct JOIN t_certificate cf USING (cf_id) WHERE pg_id = ?`;

    let diklat_data = await new Promise((resolve) =>
        connection.query(sql_diklat,
            [dataPgId],
            function(error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                return rows.length > 0 ? resolve(rows) : resolve([]);
            })
    );

    let sql_reminder_promotion = `SELECT rm_is_remindered, rm_status_reminder FROM t_reminder WHERE pg_id = ? AND rm_type = "1"`;

    let reminder_data_promotion = await new Promise((resolve) =>
        connection.query(sql_reminder_promotion,
            [dataPgId],
            function(error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                return rows.length > 0 ? resolve(rows[0]) : resolve([]);
            })
    );

    let sql_reminder_retirement = `SELECT rm_is_remindered, rm_status_reminder FROM t_reminder WHERE pg_id = ? AND rm_type = "2"`;

    let reminder_data_retirement = await new Promise((resolve) =>
        connection.query(sql_reminder_retirement,
            [dataPgId],
            function(error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                return rows.length > 0 ? resolve(rows[0]) : resolve([]);
            })
    );

    let user_data = {
        unit: data.unit,
        sub_unit: data.sub_unit,
        name: data.name,
        picture: data.picture,
        cuti: data.cuti,
        ktp: data.ktp,
        kk: data.kk,
        npwp: data.npwp,
        kartu_pegawai: data.kartu_pegawai,
        ijazah: data.ijazah,
        first_name: data.first_name,
        last_name: data.last_name,
        first_degree: data.first_degree,
        last_degree: data.last_degree,
        blood_type: data.blood_type,
        address: data.address,
        nik: data.nik,
        nip: data.nip,
        email: data.email,
        role: data.role,
        birth_place: data.birth_place,
        birth_date: data.birth_date,
        gender: data.gender,
        religion: data.religion,
        marital_status: data.marital_status,
        num_of_child: data.num_of_child,
        jabatan: {
            nama: data.jabatan_nama,
            keputusan: data.jabatan_keputusan,
            tanggal: data.jabatan_tanggal
        },
        golongan: {
            nama: data.golongan_nama,
            ruang: data.golongan_ruang,
            tanggal: data.golongan_tanggal
        },
        education: data_education,
        diklat: diklat_data,
        date_promotion: data.pg_date_promotion_next,
        date_retire: data.pg_date_retire,
        position_type: data.position_type,
        reminder_promotion_sent: reminder_data_promotion.rm_is_remindered == 0 ? false : true,
        reminder_retirement_sent: reminder_data_retirement.rm_is_remindered == 0 ? false : true,
        role: data.role,
        marital_status: data.marital_status
    };

    return response.ok(user_data, '', reply);
}

async function addPegawai(request, reply) {
    let now = moment().format('YYYY-MM-DD HH:mm:ss').toString();

    let pg_nip = request.body.nip;
    let pg_paid_leave = request.body.cuti;
    let pg_email = request.body.email;
    let pg_password = "kanakecangnagen";
    let pg_first_name = request.body.first_name;
    let pg_last_name = request.body.last_name;
    let pg_first_degree = request.body.first_degree;
    let pg_last_degree = request.body.last_degree;
    let pg_blood_type = getBloodType(request.body.blood_type);
    let pg_address = request.body.address;
    let pg_nik = request.body.nik;
    let pg_fullname = pg_first_degree + " " + pg_first_name + " " + pg_last_name + ", " + pg_last_degree;
    let pg_date_birthday = request.body.birth_date;
    let pg_birth_place = request.body.birth_place;
    let pg_gender = getGender(request.body.gender);
    let pg_religion = getReligion(request.body.religion);
    let pg_marital_status = getMaritalStatus(request.body.marital_status);
    let pg_num_of_child = request.body.num_of_child;
    let pg_position_type = getPositionType(request.body.position_type);
    let rl_id = request.body.role_id;
    let su_id = request.body.sub_unit_id;

    // Retirement Date
    console.log("RETIRE NEW " + getRetirementDate(pg_date_birthday.substring(0, 10), pg_position_type));
    console.log("RETIRE NEW " + formatDate(getRetirementDate(pg_date_birthday.substring(0, 10), pg_position_type)));
    let pg_date_retire = formatDate(getRetirementDate(pg_date_birthday.substring(0, 10), pg_position_type));

    let gr_id = request.body.current_golongan_id;
    let ga_date = request.body.current_golongan_date.substring(0, 9);

    let sql_golongan = `INSERT INTO t_grade_assignment (ga_date, ga_status, gr_id, pg_id, created_at, updated_at)
    VALUES (?, ?, ?, ?, ?, ?)`;

    let pa_name = request.body.jabatan_name;
    let pa_keputusan = request.body.jabatan_sk_name;
    let pa_date = request.body.jabatan_sk_date;

    // Next Promotion Date
    console.log("PROMOTION DATE NEW " + getRegularPromotionDate(pa_date));
    console.log("PROMOTION DATE NEW " + formatDate(getRegularPromotionDate(pa_date)));
    let pg_date_promotion = formatDate(getRegularPromotionDate(pa_date));

    let sql_jabatan = `INSERT INTO t_position_assignment (pa_name, pa_keputusan, pa_date, pa_status, pg_id, created_at, updated_at)
    VALUES (?, ?, ?, ?, ?, ?, ?)`;

    let education = request.body.education;
    let diklat = request.body.diklat;

    let pg_id = 0;

    let sql = `INSERT INTO t_pegawai 
        (pg_nip, pg_email, pg_password, pg_first_name, pg_last_name, pg_first_degree, pg_last_degree, pg_blood_type, pg_address, pg_nik, pg_full_name, pg_date_birthday, pg_date_promotion_next,
            pg_date_retire, pg_birth_place, pg_religion, pg_marital_status, pg_gender, pg_num_of_child, pg_position_type, pg_status_account,
            rl_id, su_id, created_at, updated_at, pg_profile_pic, pg_paid_leave)
        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
                ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
                ?, ?, ?, ?, ?, ?)`;
    
    let data = await new Promise((resolve) =>
    connection.query(sql,
        [pg_nip, pg_email, pg_password, pg_first_name, pg_last_name, pg_first_degree, pg_last_degree, pg_blood_type, pg_address, pg_nik, pg_fullname, pg_date_birthday.substring(0, 10), pg_date_promotion,
            pg_date_retire, pg_birth_place, pg_religion, pg_marital_status, pg_gender, pg_num_of_child, pg_position_type, "Belum Aktivasi",
            rl_id, su_id, now, now, '', pg_paid_leave], function (error, rows) {
            if (error) {
                console.log(error);
                if (`${error}`.includes("Duplicate")) {
                    return response.ok(true, "Pegawai created!", reply);
                }
                return response.badRequest('', `${error}`, reply)
            }

            pg_id = rows.insertId;

            console.log("LAST PG ID " + rows.insertId);
            console.log("NEW EMPLOYEE ADDED " + JSON.stringify(rows));
            console.log("AFFECTED " + rows.affectedRows + "\n");

            return rows.affectedRows > 0 ? resolve(true) : resolve(false);
        })
    );

    console.log("OJIG PEGAWAI CREATED TEU CIK: " + data);

    let data_golongan = await new Promise((resolve) =>
            connection.query(sql_golongan,
                [ga_date, "Aktif", gr_id, pg_id, now, now], function (error, rows) {
                    if (error) {
                        console.log(error);
                        return response.badRequest('', `${error}`, reply)
                    }
        
                    console.log("Data Golongan AFFECTED " + rows.affectedRows + "\n");
        
                    return rows.affectedRows > 0 ? resolve(true) : resolve(false);
                })
            );

    let data_jabatan = await new Promise((resolve) =>
    connection.query(sql_jabatan,
        [pa_name, pa_keputusan, pa_date, "Aktif", pg_id, now, now], function (error, rows) {
            if (error) {
                console.log(error);
                return response.badRequest('', `${error}`, reply)
            }

            console.log("Data Jabatan AFFECTED " + rows.affectedRows + "\n");

            return rows.affectedRows > 0 ? resolve(true) : resolve(false);
        })
    );

    sql_reminder = `INSERT INTO t_reminder 
    (rm_type, rm_is_remindered, rm_status_reminder, pg_id, created_at, updated_at) 
    VALUES
    ('1', '0', '1', ?, ?, ?),
    ('2', '0', '1', ?, ?, ?);`;

    let data_reminder = await new Promise((resolve) =>
    connection.query(sql_reminder,
        [pg_id, now, now, pg_id, now, now], function (error, rows) {
            if (error) {
                console.log(error);
                return response.badRequest('', `${error}`, reply)
            }

            console.log("Data Reminder AFFECTED " + rows.affectedRows + "\n");

            return rows.affectedRows > 0 ? resolve(true) : resolve(false);
        })
    );

    sql_documents = `INSERT INTO t_documents 
    (dc_type, dc_url, pg_id, created_at, updated_at) 
    VALUES
    (1, '', ?, ?, ?),
    (2, '', ?, ?, ?),
    (3, '', ?, ?, ?),
    (4, '', ?, ?, ?);`;

    let data_documents = await new Promise((resolve) =>
    connection.query(sql_documents,
        [pg_id, now, now, pg_id, now, now, pg_id, now, now, pg_id, now, now], function (error, rows) {
            if (error) {
                console.log(error);
                return response.badRequest('', `${error}`, reply)
            }

            console.log("Data Documents AFFECTED " + rows.affectedRows + "\n");

            return rows.affectedRows > 0 ? resolve(true) : resolve(false);
        })
    );

    var msg = "Pegawai created!";
    if (!data) {
        msg = "Failed to create pegawai!";
    }
    return response.ok(data, msg, reply);
}

async function updatePegawai(request, reply) {
    let now = moment().format('YYYY-MM-DD HH:mm:ss').toString();

    console.log(`\n\n ASUPPPPPP ---- ${request.body} ----- \n`);
    let pg_nip = request.body.nip;

    console.log(`\n\n ${pg_nip} \n\n`);

    let sqlPgId = `SELECT pg_id FROM t_pegawai WHERE pg_nip = ?`;

    let dataPgId = await new Promise((resolve) =>
        connection.query(sqlPgId,
            [pg_nip],
            function(error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                return rows.length > 0 ? resolve(rows[0].pg_id) : resolve('');
            })
    );

    console.log('PG ID ' + dataPgId);

    let pg_email = request.body.email;
    let pg_paid_leave = request.body.cuti;
    let pg_first_name = request.body.first_name;
    let pg_last_name = request.body.last_name;
    let pg_first_degree = request.body.first_degree;
    let pg_last_degree = request.body.last_degree;
    let pg_blood_type = getBloodType(request.body.blood_type);
    let pg_address = request.body.address;
    let pg_nik = request.body.nik;
    let pg_fullname = pg_first_degree + " " + pg_first_name + " " + pg_last_name + ", " + pg_last_degree;
    let pg_date_birthday = request.body.birth_date;
    let pg_birth_place = request.body.birth_place;
    let pg_gender = getGender(request.body.gender);
    let pg_religion = getReligion(request.body.religion);
    let pg_marital_status = getMaritalStatus(request.body.marital_status);
    let pg_num_of_child = request.body.num_of_child;
    let pg_position_type = getPositionType(request.body.position_type);
    let rl_id = request.body.role_id;
    let su_id = request.body.sub_unit_id;

    // Retirement Date
    console.log("RETIRE NEW " + getRetirementDate(pg_date_birthday.substring(0, 10), pg_position_type));
    console.log("RETIRE NEW " + formatDate(getRetirementDate(pg_date_birthday.substring(0, 10), pg_position_type)));
    let pg_date_retire = formatDate(getRetirementDate(pg_date_birthday.substring(0, 10), pg_position_type));

    let gr_id = request.body.current_golongan_id;
    let ga_date = getSubstringDate(request.body.current_golongan_date);

    let sql_golongan = `UPDATE t_grade_assignment SET ga_date = ?, ga_status = ?, gr_id = ?, updated_at = ? WHERE pg_id = ?`;

    let pa_name = request.body.jabatan_name;
    let pa_keputusan = request.body.jabatan_sk_name;
    let pa_date = getSubstringDate(request.body.jabatan_sk_date);

    let sql_jabatan = `UPDATE t_position_assignment SET pa_name = ?, pa_keputusan = ?, pa_date = ?, pa_status = ?, updated_at = ? WHERE pg_id = ?`;

    let education = request.body.education;
    let diklat = request.body.diklat;

    let sql = `UPDATE t_pegawai SET
        pg_email = ?, pg_first_name = ?, pg_last_name = ?, pg_full_name = ?, pg_date_birthday = ?, pg_date_retire = ?,
            pg_first_degree = ?, pg_last_degree = ?, pg_nik = ?, pg_address = ?, pg_blood_type = ?,
            pg_birth_place = ?, pg_religion = ?, pg_marital_status = ?, pg_gender = ?, pg_num_of_child = ?, pg_position_type = ?,
            rl_id = ?, su_id = ?, pg_paid_leave = ?,
            updated_at = ? WHERE pg_nip = ?`;
    
    let data = await new Promise((resolve) =>
        connection.query(sql,
            [pg_email, pg_first_name, pg_last_name, pg_fullname, pg_date_birthday.substring(0, 10), pg_date_retire,
                pg_first_degree, pg_last_degree, pg_nik, pg_address, pg_blood_type,
                pg_birth_place, pg_religion, pg_marital_status, pg_gender, pg_num_of_child, pg_position_type,
                rl_id, su_id, pg_paid_leave,
                now, pg_nip], function (error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('dddd', `${error}`, reply)
                }

                console.log("EMPLOYEE UPDATED " + JSON.stringify(rows));
                console.log("AFFECTED " + rows.affectedRows + "\n");

                return rows.affectedRows > 0 ? resolve(true) : resolve(false);
            })
    );

    let data_golongan = await new Promise((resolve) =>
            connection.query(sql_golongan,
                [ga_date, "Aktif", gr_id, now, dataPgId], function (error, rows) {
                    if (error) {
                        console.log(error);
                        return response.badRequest('', `${error}`, reply)
                    }
        
                    console.log("Data Golongan AFFECTED " + rows.affectedRows + "\n");
        
                    return rows.affectedRows > 0 ? resolve(true) : resolve(false);
                })
            );

    let data_jabatan = await new Promise((resolve) =>
    connection.query(sql_jabatan,
        [pa_name, pa_keputusan, pa_date, "Aktif", now, dataPgId], function (error, rows) {
            if (error) {
                console.log(error);
                return response.badRequest('', `${error}`, reply)
            }

            console.log("Data Jabatan AFFECTED " + rows.affectedRows + "\n");

            return rows.affectedRows > 0 ? resolve(true) : resolve(false);
        })
    );

    var msg = "Pegawai updated!";
    if (!data) {
        msg = "Failed to update pegawai!";
    }
    return response.ok(data, msg, reply);
}

async function deletePegawai(request, reply) {
    let nip = request.body.nip;

    let sql = "DELETE FROM t_pegawai WHERE pg_nip = ?";
    let data = await new Promise((resolve) =>
        connection.query(sql,
            [nip], function (error, rows) {
                if(error){
                    console.log(error);
                    return response.badRequest('', `${error}`, reply);
                }

                console.log("DELETE EMPLOYEE AFFECTED " + rows.affectedRows + "\n");

                return rows.affectedRows > 0 ? resolve(true) : resolve(false);
            })
    );

    console.log("OJIG " + data);

    var msg = "Berhasil mengapus data pegawai!";
    if (!data) {
        msg = "Gagal menghapus data pegawai!";
    }
    return response.ok(data, msg, reply);
}

async function getRole(request, reply) {
    let sql = `SELECT rl_id, rl_name FROM t_role`;

    let data = await new Promise((resolve) => connection.query(sql, [], function (error, rows) {
        if (error) {
            console.log(error);
            return response.badRequest('', `${error}`, reply)
        }

        if (rows.length > 0) {
            let array = [];
            rows.forEach(element => {
                array.push({
                    id: element.rl_id,
                    name: element.rl_name
                });
            });

            return resolve(array);
        }
        else {
            return resolve([]);
        }
    })
    );

    return response.ok(data, '', reply);
}

async function getUnit(request, reply) {
    let sql = `SELECT un_id, un_name FROM t_unit`;

    let data = await new Promise((resolve) => connection.query(sql, [], function (error, rows) {
        if (error) {
            console.log(error);
            return response.badRequest('', `${error}`, reply)
        }

        if (rows.length > 0) {
            let array = [];
            rows.forEach(element => {
                array.push({
                    id: element.un_id,
                    name: element.un_name
                });
            });

            return resolve(array);
        }
        else {
            return resolve([]);
        }
    })
    );

    return response.ok(data, '', reply);
}

async function addUnit(request, reply) {
    let now = moment().format('YYYY-MM-DD HH:mm:ss').toString();
    let name = request.body.unit_name;
    let sql = `INSERT INTO t_unit (un_name, created_at, updated_at) 
                VALUES(?, ?, ?)`;
    let data = await new Promise((resolve) =>
        connection.query(sql,
            [name, now, now], function (error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                console.log("AFFECTED " + rows.affectedRows + "\n");

                return rows.affectedRows > 0 ? resolve(true) : resolve(false);
            })
    );

    var msg = "Unit created!";
    if (!data) {
        msg = "Failed to create unit!";
    }
    return response.ok(data, msg, reply);
}

async function getGrade(request, reply) {
    let sql = `SELECT gr_id, gr_name, gr_group FROM t_grade`;

    let data = await new Promise((resolve) => connection.query(sql, [], function (error, rows) {
        if (error) {
            console.log(error);
            return response.badRequest('', `${error}`, reply)
        }

        if (rows.length > 0) {
            let array = [];
            rows.forEach(element => {
                array.push({
                    id: element.gr_id,
                    name: element.gr_name,
                    golongan: element.gr_group
                });
            });

            return resolve(array);
        }
        else {
            return resolve([]);
        }
    })
    );

    return response.ok(data, '', reply);
}

async function updatePositionTypeByNip(request, reply) {

    let now = moment().format('YYYY-MM-DD HH:mm:ss').toString();
    let updated_at = now;
    let pg_nip = request.body.nip;

    let pos_type_sql = `SELECT pg_position_type FROM t_pegawai WHERE pg_nip = ?`;

    let position_type = await new Promise((resolve) => connection.query(pos_type_sql, [pg_nip], function (error, rows) {
        if (error) {
            console.log(error);
            return response.badRequest('', `${error}`, reply)
        }

        if (rows.length > 0) {
            return resolve(rows[0].pg_position_type);
        }
        else {
            return resolve("");
        }
    })
    );

    let type = "Struktural";

    if (position_type == "Struktural") {
        type = "Fungsional";
    }

    console.log("Update position type from -" + position_type + "- to -" + type);

    let sql = `UPDATE t_pegawai set pg_position_type = ?, updated_at = ? 
    WHERE pg_nip = ?`;

    let data = await new Promise((resolve) =>
        connection.query(sql,
            [type, updated_at, pg_nip], 
                function (error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                console.log('AFFECTED UPDATE Position Type ' + rows.affectedRows);

                return rows.affectedRows > 0 ? resolve(true) : resolve(false);
            })
    );

    let msg = data ? "Berhasil update positon type!" : "Gagal update positon type!";
    return response.ok(data, msg, reply);
}

function getRetirementDate(birthDate, posType) {
    let bd = new Date(birthDate);
    if (posType == "Fungsional") {
        let retireDate = new Date();
        retireDate.setFullYear(bd.getFullYear() + 58);
        return retireDate;
    }
    let retireDate = new Date();
    retireDate.setFullYear(bd.getFullYear() + 65);
    return retireDate;

    // console.log("---Today " + new Date());
    // console.log("---" + pg_date_birthday + " toDate " + new Date(pg_date_birthday));
    // let today = new Date();
    // let birthDate = new Date(pg_date_birthday);
    // console.log("---Difference " + (today.getFullYear() - birthDate.getFullYear()));
    // let age = today.getFullYear() - birthDate.getFullYear();
    // let m = today.getMonth() - birthDate.getMonth();
    // if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    //     age--;
    // }
    // console.log("---AGE " + age + " " + Number.isInteger(age));
    // console.log("---Retire in " + (58 - age) + " years");
    // let retireDate = new Date();
    // retireDate.setFullYear(birthDate.getFullYear() + 58);
    // console.log("---Retire at " + retireDate);
}

function getRegularPromotionDate(lastPromotionDate) {
    let bd = new Date(lastPromotionDate);
    let promotionDate = new Date();
    promotionDate.setFullYear(bd.getFullYear() + 4);
    return promotionDate;
}

function formatDate(d) {
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-').toString();
}

function getBloodType(blood_type) {
    if (blood_type == 1) {
        return "O";
    } else if (blood_type == 2) {
        return "A";
    } else if (blood_type == 3) {
        return "B";
    } else {
        return "AB";
    }
}

function getGender(gender) {
    if (gender == 1) {
        return "L";
    } else {
        return "P";
    }
}

function getReligion(religion) {
    if (religion == 1) {
        return "Islam";
    } else if (religion == 2) {
        return "Kristen";
    } else if (religion == 3) {
        return "Katolik";
    } else if (religion == 4) {
        return "Hindu";
    } else if (religion == 5) {
        return "Buddha";
    } else {
        return "Kong Hu Cu";
    }
}

function getMaritalStatus(marital) {
    if (marital == 1) {
        return "Kawin";
    } else {
        return "Belum Kawin";
    }
}

function getPositionType(position) {
    if (position == 1) {
        return "Fungsional";
    } else {
        return "Struktural";
    }
}

function getSubstringDate(date) {
    if (date.includes('Z')) {
        return date.substring(0, 10);
    } else {
        return date;
    }
}

module.exports = {
    getAllEmployee, getEmployeeByNip, addPegawai, updatePegawai, deletePegawai, getGrade, addUnit, getUnit, getRole, updatePositionTypeByNip
};