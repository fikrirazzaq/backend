let response = require('../response');
let connection = require('../connection');
let sha1 = require('sha1');
let moment = require('moment');
let crypto = require('crypto');
let header = require('../helpers/token');

var MailConfig = require('../config/email');
var hbs = require('nodemailer-express-handlebars');
var gmailTransport = MailConfig.GmailTransport;

async function readNotifAdmin(request, reply) {
    let token = request.headers.authorization;
    let check = await header.check(token, reply);
    let now = moment().format('YYYY-MM-DD HH:mm:ss').toString();

    let sql = `UPDATE t_notif_admin set na_status_read = 1, na_time_read = ? WHERE na_id = ?`;

    let data = await new Promise((resolve) =>
        connection.query(sql,
            [now, request.body.notif_id], function (error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                console.log("AFFECTED " + rows.affectedRows);

                return rows.affectedRows > 0 ? resolve(true) : resolve(false);
            })
    );

    let sqlUpdate = `INSERT INTO t_notif_admin_read (nr_read_status, nr_read_time, na_id, pg_id) 
    VALUES ('1', ?, ?, ?)`;

    let dataUpdate = await new Promise((resolve) =>
        connection.query(sqlUpdate,
            [now, request.body.notif_id, check.pg_id], function (error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                console.log("UPDATE READ ADMIN AFFECTED " + rows.affectedRows);

                return rows.affectedRows > 0 ? resolve(true) : resolve(false);
            })
    );

    let msg = data && dataUpdate ? "Read status updated!" : "Read status update failed!";
    return response.ok(data, msg, reply);
}

async function readNotifPegawai(request, reply) {
    // Update Read Status
    let now = moment().format('YYYY-MM-DD HH:mm:ss').toString();

    let sql = `UPDATE t_notif_pegawai set np_status_read = 1, np_time_read = ? WHERE np_id = ?`;

    let data = await new Promise((resolve) =>
        connection.query(sql,
            [now, request.body.notif_id], function (error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                console.log("AFFECTED " + rows.affectedRows);

                return rows.affectedRows > 0 ? resolve(true) : resolve(false);
            })
    );

    let msg = data ? "Read status updated!" : "Read status update failed!";
    return response.ok(data, msg, reply);
}

/**
 * 
 * @param {*} request 
 * @param {*} reply 
 * 
 * 1. Update na_valid_status
 * 2. Add Notif Pegawai
 * 3. Send Email
 */
async function validNotifAdmin(request, reply) {
    let token = request.headers.authorization;
    let check = await header.check(token, reply);

    let notif_id = request.body.notif_id;
    let pg_nip = request.body.nip;
    let np_type = request.body.notif_type;

    let now = moment().format('YYYY-MM-DD HH:mm:ss').toString();

    let sql = `UPDATE t_notif_admin set na_valid = 1, na_time_valid = ? WHERE na_id = ?`;

    let data = await new Promise((resolve) =>
        connection.query(sql,
            [now, notif_id], function (error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                console.log("AFFECTED " + rows.affectedRows);

                return rows.affectedRows > 0 ? resolve(true) : resolve(false);
            })
    );

    let sqlUpdate = `INSERT INTO t_notif_admin_validasi (nv_valid_status, nv_valid_time, na_id, pg_id) 
    VALUES ('1', ?, ?, ?)`;

    let dataUpdate = await new Promise((resolve) =>
        connection.query(sqlUpdate,
            [now, notif_id, check.pg_id], function (error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                console.log("UPDATE VALIDASI ADMIN AFFECTED " + rows.affectedRows);

                return rows.affectedRows > 0 ? resolve(true) : resolve(false);
            })
    );

    let sqlPgId = `SELECT pg_id FROM t_pegawai WHERE pg_nip = ?`;

    let dataPgId = await new Promise((resolve) =>
        connection.query(sqlPgId,
            [pg_nip],
            function(error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                return rows.length > 0 ? resolve(rows[0].pg_id) : resolve('');
            })
    );

    console.log('PG ID ' + dataPgId);
    

    // Insert Notif to Notif Pegawai

    var np_name = "";
    var np_content = "";
    switch(np_type) {
        case "1":
            np_name = "Notifikasi Promosi Jabatan";
            np_content = "Silahkan lengkapi persyaratan untuk kenaikan pangkat.";            
            break;
        case "2":
            np_name = "Notifikasi Pensiun";
            np_content = "Silahkan lengkapi persyaratan untuk kenaikan pangkat.";
            break;
        case "3":
            np_name = "Notifikasi Ulang Tahun";
            np_content = "Atep ulang tahun hari ini.";
            break;
        default:
            np_name = "Notifikasi Promosi Jabatan";
            np_content = "Silahkan lengkapi persyaratan untuk kenaikan pangkat.";
            break;
    }

    let sqlInsertNoitf = `INSERT INTO t_notif_pegawai 
            (np_type, np_name, np_content, np_status_read, np_time_received, pg_id) 
            VALUES (?, ?, ?, '0', ?, ?)`;
    let dataInsert = await new Promise((resolve) =>
        connection.query(sqlInsertNoitf,
            [np_type, np_name, np_content, now, dataPgId], function (error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                console.log("Insert notif employee success " + rows.affectedRows);

                return rows.affectedRows > 0 ? resolve(true) : resolve(false);
            })
    );

    // Send Notif to Email Pegawai

    MailConfig.ViewOption(gmailTransport,hbs);
    let HelperOptions = {
        from: '"Kemnaker TU" <kemnaker.tu@gmail.com>',
        to: 'juveticsatu@gmail.com',
        subject: 'TEDASDASD!',
        template: 'email',
        context: {
        name:"DOLPO ANGSA",
        email: "buatrosinye.tu@gmail.com",
        address: "ASDASDASDASDASD"
        }
    };
    gmailTransport.sendMail(HelperOptions, (error,info) => {
        if(error) {
        console.log(error);
        reply.send({ message: error, code: 400 });
        }
        console.log("email is send");
        console.log(info);
        reply.send({code: 200, message: 'Email sent!', values: info});
    });

    let msg = data && dataUpdate && dataInsert? "Validasi status updated!" : "Validasi status update failed!";
    return response.ok(data, msg, reply);
}

async function showNotifPegawai(request, reply) {
    let token = request.headers.authorization;
    let check = await header.check(token, reply);
    let sql = `SELECT np.np_id, np.np_type, np.np_name, np.np_content, np.np_status_read, 
    np.np_time_received, np.np_time_read, np.pg_id, pg.pg_nip, pg.pg_full_name
    FROM t_notif_pegawai np
    JOIN t_pegawai pg USING (pg_id)
    WHERE pg_id = ?
    ORDER BY np_time_received DESC`;

    let data = await new Promise((resolve) => connection.query(sql, [check.pg_id], function (error, rows) {
        if (error) {
            console.log(error);
            return response.badRequest('', `${error}`, reply)
        }

        if (rows.length > 0) {
            let array = [];
            rows.forEach(element => {
                array.push({
                    notif_id: element.np_id,
                    notif_type: element.np_type,
                    notif_name: element.np_name,
                    notif_content: element.np_content,
                    notif_time_received: element.np_time_received,
                    read: {
                        read_time: element.np_time_read,
                        read_status: (element.np_status_read == 0) ? false : true,
                    }
                });
            });

            return resolve(array);
        }
        else {
            return resolve([]);
        }
    })
    );

    return response.ok(data, '', reply);
}

async function showNotifAdmin(request, reply) {
    let sql = `SELECT * FROM (
        SELECT na.na_id, na.na_type, na.na_name, na.na_content, na.na_status_read, na.na_valid, 
    na.na_time_received, na.na_time_read, na.na_time_valid, na.pg_id as employee_id, nr.pg_id as admin_id_read, nv.pg_id as admin_id_validasi
    FROM t_notif_admin na
    LEFT JOIN t_notif_admin_read nr ON na.na_id = nr.na_id
    LEFT JOIN t_notif_admin_validasi nv ON na.na_id = nv.na_id
    UNION
        SELECT na.na_id, na.na_type, na.na_name, na.na_content, na.na_status_read, na.na_valid, 
    na.na_time_received, na.na_time_read, na.na_time_valid, na.pg_id as employee_id, nr.pg_id as admin_id_read, nv.pg_id as admin_id_validasi 
    FROM t_notif_admin na
    RIGHT JOIN t_notif_admin_read nr ON na.na_id = nr.na_id
    LEFT JOIN t_notif_admin_validasi nv ON na.na_id = nv.na_id
    ) notif ORDER BY na_time_received
`;

    let data = await new Promise((resolve) => connection.query(sql, [], function (error, rows) {
        if (error) {
            console.log(error);
            return response.badRequest('', `${error}`, reply)
        }

        if (rows.length > 0) {
            let array = fetchAllNotifAdmin(rows);
            return resolve(array);
        } else {
            return resolve([]);
        }
    })
    );

    return response.ok(data, '', reply);
}

async function fetchAllNotifAdmin(rows) {
    let array = []
    for(let element of rows) {
        try {
            let sqlEmployee = `SELECT pg_id, pg_full_name, pg_first_name, pg_last_name, pg_nip, pg_email FROM t_pegawai WHERE pg_id = ?`;

            let employeeData = await new Promise((resolve) =>
                connection.query(sqlEmployee,
                    [element.employee_id],
                    function(error, rows) {
                        if (error) {
                            console.log(error);
                            return response.badRequest('', `${error}`, reply)
                        }

                        return rows.length > 0 ? resolve({
                            name: rows[0].pg_full_name,
                            first_name: rows[0].pg_first_name,
                            last_name: rows[0].pg_last_name,
                            nip: rows[0].pg_nip,
                            email: rows[0].pg_email,
                        }) : resolve({});
                    })
            );

            let sqlAdminRead = `SELECT pg_id, pg_full_name, pg_first_name, pg_last_name, pg_nip, pg_email, na_id FROM t_pegawai JOIN t_notif_admin_read USING (pg_id) WHERE pg_id = ? AND na_id = ?`;

            let adminReadData = await new Promise((resolve) =>
                connection.query(sqlAdminRead,
                    [element.admin_id_read, element.na_id],
                    function(error, rows) {
                        if (error) {
                            console.log(error);
                            return response.badRequest('', `${error}`, reply)
                        }

                        return rows.length > 0 ? resolve({
                            name: rows[0].pg_full_name,
                            first_name: rows[0].pg_first_name,
                            last_name: rows[0].pg_last_name,
                            nip: rows[0].pg_nip,
                            email: rows[0].pg_email
                        }) : resolve({});
                    })
            );

            let sqlAdminValidasi = `SELECT pg_id, pg_full_name, pg_nip, pg_email, na_id FROM t_pegawai JOIN t_notif_admin_validasi USING (pg_id) WHERE pg_id = ? AND na_id = ?`;

            let adminValidasiData = await new Promise((resolve) =>
                connection.query(sqlAdminValidasi,
                    [element.admin_id_validasi, element.na_id],
                    function(error, rows) {
                        if (error) {
                            console.log(error);
                            return response.badRequest('', `${error}`, reply)
                        }

                        console.log('Admin Validasi ID ' + element.admin_id_validasi);
                        console.log('Notif ID ' + element.na_id);
                        console.log('ROWS ' + JSON.stringify(rows[0]))

                        return rows.length > 0 ? resolve({
                            name: rows[0].pg_full_name,
                            nip: rows[0].pg_nip,
                            email: rows[0].pg_email
                        }) : resolve({});
                    })
            );

            array.push({
                notif_id: element.na_id,
                notif_type: element.na_type,
                notif_name: element.na_name + ": " + employeeData.name,
                notif_content: element.na_content,
                notif_time_received: element.na_time_received,
                receiver: employeeData,
                read: {
                    read_time: element.na_time_read,
                    read_status: (element.na_status_read == 0) ? false : true
                },
                reader: adminReadData
            });

        } catch (error) {
            console.log('error'+ error);
        }
    }
    return array
}

async function sendNotifReminder(request, reply) {
    // Send Notif to Email Pegawai
    let now = moment().format('YYYY-MM-DD HH:mm:ss').toString();
    let type = request.body.type;
    let email = request.body.email;
    let subject = request.body.subject;
    let nip = request.body.nip;
    let content = request.body.content;

    let sqlPgId = `SELECT pg_id FROM t_pegawai WHERE pg_nip = ?`;

    let dataPgId = await new Promise((resolve) =>
        connection.query(sqlPgId,
            [nip],
            function(error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                return rows.length > 0 ? resolve(rows[0].pg_id) : resolve('');
            })
    );

    console.log('PG ID ' + dataPgId);

    MailConfig.ViewOption(gmailTransport,hbs);
    let HelperOptions = {
        from: '"Kemnaker TU" <kemnaker.tu@gmail.com>',
        to: email,
        subject: subject,
        template: 'email',
        context: {
            title: "Reminder " + subject,
            name: nip,
            email: email,
            address: content
        }
    };
    gmailTransport.sendMail(HelperOptions, (error,info) => {
        if(error) {
        console.log(error);
        reply.send({ message: error, code: 400 });
        }
        console.log("email is send");
        console.log(info);
        reply.send({code: 200, message: 'Email sent!', values: info});
    });

    let sqlInsertNoitf = `INSERT INTO t_notif_admin 
        (na_type, na_name, na_content, na_status_read, na_valid, na_time_received, pg_id) 
        VALUES (?, ?, ?, ?, ?, ?, ?)`;
    let dataInsert = await new Promise((resolve) =>
        connection.query(sqlInsertNoitf,
            [type, subject, content, 0, 0, now, dataPgId], function (error, rows) {
                if (error) {
                    console.log(error);
                    return response.badRequest('', `${error}`, reply)
                }

                console.log("Insert notif admin success " + rows.affectedRows);

                return rows.affectedRows > 0 ? resolve(true) : resolve(false);
            })
    );

    let msg = dataInsert ? "Notif sent!" : "Notif failed!";
    return response.ok(dataInsert, msg, reply);
}

module.exports = {
    showNotifAdmin, showNotifPegawai, readNotifPegawai, readNotifAdmin, validNotifAdmin, sendNotifReminder
};