let response = require('../response');
let connection = require('../connection');
let sha1 = require('sha1');
let moment = require('moment');
let crypto = require('crypto');
let header = require('../helpers/token');

var MailConfig = require('../config/email');
var hbs = require('nodemailer-express-handlebars');
var gmailTransport = MailConfig.GmailTransport;

async function getEmployeeReport(request, reply) {

    let sql = `SELECT un_id, un_name FROM t_unit`;

    let data = await new Promise((resolve) => connection.query(sql, [], function(error, rows) {
        if (error) {
            console.log(error);
            return response.badRequest('', `${error}`, reply)
        }

        if (rows.length > 0) {
            let array = fetchSubUnit(rows);
            return resolve(array);
        } else {
            return resolve([]);
        }
    }));

    return response.ok(data, '', reply);
}

async function fetchSubUnit(rows) {
    let arraySub = [];
    for(let element of rows) {
        try {
            let sqlSubUnit = `SELECT su_id, su_name FROM t_sub_unit WHERE un_id = ?`;

            let subUnitData = await new Promise((resolve) =>
                connection.query(sqlSubUnit,
                    [element.un_id],
                    function(error, rows) {
                        if (error) {
                            console.log(error);
                            return response.badRequest('', `${error}`, reply)
                        }

                        if (rows.length > 0) {
                            let subUnitArray = fetchEmployeeBySubUnit(rows);
                            return resolve(subUnitArray);
                        } else {
                            return resolve([]);
                        }
                    })
            );

            arraySub = subUnitData;
        } catch (error) {
            console.log('error'+ error);
        }
    }
    return arraySub;
}

async function fetchEmployeeBySubUnit(rows) {
    let array = []
    for(let element of rows) {
        try {
            let sqlEmployee = `SELECT s.su_name, u.un_name, p.pg_ktp, p.pg_kk, p.pg_kartu_pegawai, p.pg_npwp, p.pg_ijazah, p.pg_id, p.pg_first_degree, p.pg_last_degree, p.pg_blood_type, p.pg_nik, p.pg_address, p.pg_email, p.pg_full_name, p.pg_first_name, p.pg_last_name, p.pg_nip, p.pg_position_type, p.pg_date_retire, p.pg_date_promotion_next, p.pg_birth_place, p.pg_date_birthday, pa.pa_name, pa.pa_keputusan, pa.pa_date, gr.gr_name, gr.gr_group, ga.ga_date, p.pg_religion, p.pg_marital_status, p.pg_gender, p.pg_num_of_child, p.pg_profile_pic, p.pg_paid_leave
            FROM t_pegawai p
            LEFT JOIN t_sub_unit s ON p.su_id = s.su_id
            LEFT JOIN t_unit u ON s.un_id = u.un_id
            LEFT JOIN t_position_assignment pa ON p.pg_id = pa.pg_id
            LEFT JOIN t_grade_assignment ga ON p.pg_id = ga.pg_id
            LEFT JOIN t_grade gr ON ga.gr_id = gr.gr_id
            WHERE s.su_id = ?`;

            let schoolData = await new Promise((resolve) =>
                connection.query(sqlEmployee,
                    [element.su_id],
                    function(error, rows) {
                        if (error) {
                            console.log(error);
                            return response.badRequest('', `${error}`, reply)
                        }

                        if (rows.length > 0) {
                            let subUnitArray = fetchEmployeeBySubUnit(rows);
                            return resolve(subUnitArray);
                        } else {
                            return resolve([]);
                        }
                    })
            );

            if (element.pg_nip != null) {
                array.push({
                    unit: element.un_name,
                    sub_unit: element.su_name,
                    name: element.pg_full_name,
                    picture: element.pg_profile_pic,
                    cuti: element.pg_paid_leave,
                    ktp: element.pg_ktp,
                    kk: element.pg_kk,
                    npwp: element.pg_npwp,
                    kartu_pegawai: element.pg_kartu_pegawai,
                    ijazah: element.pg_ijazah,
                    first_name: element.pg_first_name,
                    last_name: element.pg_last_name,
                    first_degree: element.pg_first_degree,
                    last_degree: element.pg_last_degree,
                    blood_type: element.pg_blood_type,
                    address: element.pg_address,
                    nik: element.pg_nik,
                    nip: element.pg_nip,
                    email: element.pg_email,
                    role: element.rl_id,
                    birth_place: element.pg_birth_place,
                    birth_date: element.pg_date_birthday,
                    gender: element.pg_gender,
                    religion: element.pg_religion,
                    marital_status: element.pg_marital_status,
                    num_of_child: element.pg_num_of_child,
                    jabatan: {
                        nama: element.pa_name,
                        keputusan: element.pa_keputusan,
                        tanggal: element.pa_date
                    },
                    golongan: {
                        nama: element.gr_name,
                        ruang: element.gr_group,
                        tanggal: element.ga_date
                    },
                    education: schoolData,
                    date_next_promotion: element.pg_date_promotion_next,
                    date_retire: element.pg_date_retire,
                    position_type: element.pg_position_type
                });
            }

        } catch (error) {
            console.log('error'+ error);
        }
    }
    return array
}

module.exports = {
    getEmployeeReport
};