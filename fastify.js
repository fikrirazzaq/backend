require('dotenv').config();

// Inisialisasi awal fastify.
const fastify = require('fastify')({
    logger: true //aktifkan ini untuk menerima log setiap request dari fastify.
});

const path = require('path')

//Fungsi ini untuk membuat kita bisa melakuakn post melalui www-url-encoded.
fastify.register(require('fastify-formbody'));

const fileUpload = require('fastify-file-upload');

fastify.register(fileUpload);

//Route yang dipisah dari root file.
fastify.register(require('./routes'));

fastify.register(require('fastify-cors'), {
    // put your options here
});

fastify.register(require('fastify-static'), {
    root: path.join(__dirname, 'storage'),
    prefix: '/storage/', // optional: default '/'
  })

// const multer = require('fastify-multer')
// fastify.register(multer.contentParser);

// fastify.register(require('fastify-multipart'));

// Scheduler

let response = require('../backend/response');
let connection = require('../backend/connection');
let moment = require('moment');

// var MailConfig = require('../backend/config/email_reminder_promotion');
// var hbs = require('nodemailer-express-handlebars');
// var gmailTransport = MailConfig.GmailTransport;

const cron = require("node-cron");
cron.schedule("0 9 * * *", function() {

    console.log("Scheduler");

    // Looping tiap pegawai
    // Check isReminded
    // Check difference date equal 180 days, or equal 90 days, or equal 60 days, or equal 30 days, or equal 14 days, or equal 7 days, or equal 5 days, 4,3,2,1
    // Insert record to Notif Admin & Reminder
    // Send Email to Admin
});

var cronMinutes = require('node-cron');
 
cronMinutes.schedule('* * * * *', () => {
  console.log('running a task every minute' + Date.now());
});

// Promotion
var cronPromotion = require('node-cron');
 
cronPromotion.schedule('2 0 * * *', () => {
   console.log('Runing a job at 01:00 at America/Sao_Paulo timezone');
 }, {
   scheduled: true,
   timezone: "Asia/Jakarta"
 });

 // Retired
 var cronRetire = require('node-cron');
 
 cronRetire.schedule('22 10 * * *', async () => {
   console.log('Runing a job at 09:24 at Asia/Jakarta timezone');
   //SELECT * FROM `t_grade_assignment` JOIN t_pegawai USING(pg_id) WHERE DATE_SUB(ga_date, INTERVAL 6 MONTH) = CURDATE()
   //SELECT * FROM `t_grade_assignment` JOIN t_pegawai USING(pg_id) WHERE DATE_SUB(ga_date, INTERVAL 3 MONTH) = CURDATE()
   //SELECT * FROM `t_grade_assignment` JOIN t_pegawai USING(pg_id) WHERE DATE_SUB(ga_date, INTERVAL 1 MONTH) = CURDATE()
   //SELECT * FROM `t_grade_assignment` JOIN t_pegawai USING(pg_id) WHERE DATE_SUB(ga_date, INTERVAL 3 WEEK) = CURDATE()
   //SELECT * FROM `t_grade_assignment` JOIN t_pegawai USING(pg_id) WHERE DATE_SUB(ga_date, INTERVAL 2 WEEK) = CURDATE()
   //SELECT * FROM `t_grade_assignment` JOIN t_pegawai USING(pg_id) WHERE DATE_SUB(ga_date, INTERVAL 1 WEEK) = CURDATE()
   //SELECT * FROM `t_grade_assignment` JOIN t_pegawai USING(pg_id) WHERE DATE_SUB(ga_date, INTERVAL 6 DAY) = CURDATE()
   //SELECT * FROM `t_grade_assignment` JOIN t_pegawai USING(pg_id) WHERE DATE_SUB(ga_date, INTERVAL 4 DAY) = CURDATE()
   //SELECT * FROM `t_grade_assignment` JOIN t_pegawai USING(pg_id) WHERE DATE_SUB(ga_date, INTERVAL 3 DAY) = CURDATE()
   //SELECT * FROM `t_grade_assignment` JOIN t_pegawai USING(pg_id) WHERE DATE_SUB(ga_date, INTERVAL 2 DAY) = CURDATE()
   //SELECT * FROM `t_grade_assignment` JOIN t_pegawai USING(pg_id) WHERE DATE_SUB(ga_date, INTERVAL 1 DAY) = CURDATE()

   let sqlPgId = `SELECT pg_id, pg_nip, pg_email, pg_full_name FROM t_pegawai JOIN t_grade_assignment USING (pg_id) WHERE DATE_SUB(ga_date, INTERVAL 6 MONTH) = CURDATE()`;

    let dataPgId = await new Promise((resolve) =>
        connection.query(sqlPgId,
            [],
            function(error, rows) {
                if (error) {
                    console.log(error);
                    // return response.badRequest('', `${error}`, reply)
                }

                if (rows.length > 0) {
                  // sendNotification(rows);
                }
                // return rows.length > 0 ? resolve(rows[0]) : resolve('');
            })
    );
 }, {
   scheduled: true,
   timezone: "Asia/Jakarta"
 });

//  async function sendNotification(rows) {
//   let sqlInsertNoitf = `INSERT INTO t_notif_admin 
//   (na_type, na_name, na_content, na_status_read, na_valid, na_time_received, pg_id) 
//   VALUES (?, ?, ?, ?, ?, ?, ?)`;
//   let now = moment().format('YYYY-MM-DD HH:mm:ss').toString();

//    for (let pegawai of rows) {
//      try {
//       let dataInsert = await new Promise((resolve) =>
//       connection.query(sqlInsertNoitf,
//           ["1", "Notifikasi Promosi Jabatan", "Silahkan lengkapi persyaratan untuk kenaikan pangkat.", 0, 0, now, pegawai.pg_id], function (error, rows) {
//               if (error) {
//                   console.log(error);
//                   // return response.badRequest('', `${error}`, reply)
//               } else {
//                 var msg = "Notifikasi Promosi Jabatan!";
//                 MailConfig.ViewOption(gmailTransport,hbs);
//                 let HelperOptions = {
//                     from: '"Kemnaker KKHI Admin" <kemnaker.tu@gmail.com>',
//                     to: pegawai.pg_email,
//                     subject: msg,
//                     template: 'email_promote_notification_admin',
//                     context: {
//                         title: "Notifikasi Promosi Jabatan",
//                         nama_pegawai: pegawai.pg_full_name,
//                         nip: pegawai.pg_nip,
//                     }
//                 };
//                 gmailTransport.sendMail(HelperOptions, (error,info) => {
//                     if(error) {
//                     console.log(error);
//                     reply.send({ message: error, code: 400 });
//                     }
//                     console.log("email is send");
//                     console.log(info);
//                     reply.send({code: 200, message: 'Email sent!', values: info});
//                 });
//                 console.log("Insert notif admin success " + rows.affectedRows);
//               }
//               // return rows.affectedRows > 0 ? resolve(true) : resolve(false);
//           })
//       );
//         console.log('asup ieu');
//      } catch (error) {
//         console.log('error'+ error);
//       }
//    }
//  }

//Fungsi file root secara async.
const start = async () => {
    try {
        //Gunakan Port dari ENV APP_PORT, kalo ngga ada variable tersebut maka akan menggunakan port 3000
        await fastify.listen(process.env.APP_PORT || 3000 , "0.0.0.0");

        fastify.log.info(`server listening on ${fastify.server.address().port}}`)
    } catch (err) {
        fastify.log.error(err);
        process.exit(1)
    }
};

//Jalankan server!
start();