let nodemailer = require('nodemailer');
require('dotenv').config();
let environment = process.env;

module.exports.GmailTransport = nodemailer.createTransport({
    service: environment.GMAIL_SERVICE_NAME,
    host: environment.GMAIL_SERVICE_HOST,
    secure:environment.GMAIL_SERVICE_SECURE,
    port: environment.GMAIL_SERVICE_PORT,
    auth: {
        user: environment.GMAIL_USER_NAME,
        pass: environment.GMAIL_USER_PASSWORD
    }
});

module.exports.ViewOption = (transport, hbs) => {
    const handlebarOptions = {
        viewEngine: {
          extName: '.hbs',
          partialsDir: 'views',
          layoutsDir: 'views',
          defaultLayout: 'email_promote_notification_pegawai.hbs',
        },
        viewPath: 'views',
        extName: '.hbs',
      };

    transport.use('compile', hbs(handlebarOptions));
}